<?php 
session_start();

include_once"config.php";
if(!isset($_SESSION['username']) || !isset($_SESSION['password'])){
	header("Location: login.php");
}else{
//$fetch_users_data = mysql_fetch_object(mysql_query("SELECT * FROM `members` WHERE username='".$_SESSION['username']."'"));
}
?>
<?php
header('Content-Type: text/html; charset=utf8_general_ci');
$id = $_GET['id'];

mysqli_set_charset($conn,"utf8");
$result = $conn->query("SELECT * FROM etiquette WHERE etiquettemulti_id = '$id'"); 

?>

<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8"/>

	<title>Detail Rental</title>
	
	<link rel="stylesheet" href="css/layout.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="css/style.css">
	<!--[if lt IE 9]>
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script src="js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script src="js/hideshow.js" type="text/javascript"></script>
<script src="js/jquery.tablesorter.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery.equalHeight.js"></script>
<script type="text/javascript">
	$(document).ready(function() 
	{ 
		$(".tablesorter").tablesorter(); 
	} 
	);
	$(document).ready(function() {

	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});
</script>

<script type="text/javascript">
	$(function(){
		$('.column').equalHeight();
	});
</script>
<!-- Function for Showing FileUpload option -->
<script type="text/javascript">
	function ShowHideDiv(chkImage) {
		var dvPassport = document.getElementById("dvPassport");
		dvPassport.style.display = chkImage.checked ? "block" : "none";
	}
	function ShowHideDivLogo(chkLogo) {
		var dvPassport = document.getElementById("dvPassportlogo");
		dvPassport.style.display = chkLogo.checked ? "block" : "none";
	}

	function ShowHideDivChin(chkImage) {
			var dvChin = document.getElementById("dvChin");
		dvChin.style.display = chkImage.checked ? "block" : "none";

	}

	function ShowHideDivJap(chkImage) {
			var dvJap = document.getElementById("dvJap");
		dvJap.style.display = chkImage.checked ? "block" : "none";
	}
</script>
</head>


<body>

	<header id="header">
		<hgroup>
			<h1 class="site_title"><a href="index.php">Feejee Traveller</a></h1>
			<h2 class="section_title">Detail Rental</h2>
		</hgroup>
	</header> <!-- end of header bar -->
	
	<section id="secondary_bar">
		<div class="user">
			<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
		</div>
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="index.php">Feejee Traveller</a> <div class="breadcrumb_divider"></div> <a class="current">Detail Rental</a></article>
		</div>
	</section><!-- end of secondary bar -->
	
	<?php include('navBar.php'); ?>
	
	<section id="main" class="column">

		<div class="clear"></div>
		
		<article class="module width_full">
			<header><h3 class="tabs_involved">Rental</h3>
				<ul class="tabs">
					<li><a href="#tab1">English</a></li>
					<li><a href="#tab2">Chinese</a></li>
					<li><a href="#tab3">Japanese</a></li>

				</ul>
			</header>
			
			
		<div class="tab_container">
				<div id="tab1" class="tab_content">
				<header><h3>English</h3></header>
					<form action="editrentals.php?id=<?php echo $id;?>&lang=<?php echo "1";?>" method="post" enctype="multipart/form-data">
						<?php 		
							$varLang=1;
							$result = $conn->query("SELECT * FROM rentals WHERE rentalmulti_id = '$id' and language_lang_id='1'"); 
	
							$row = $result->fetch_assoc();
							
						 ?>
							<fieldset style="width:48%; float:left; margin-right: 3%;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>rental Name </label>
								<input type="text" name="rental_name" value="<?php echo $row['rental_name']; ?>" style="width:92%;" > 
							</fieldset>
														<!-- Another Row -->
							<fieldset style="width:48%; float:right;">
								<label>Rental Price</label>
								<input type="text" name="rental_price" value="<?php echo $row['rental_price']; ?>" style="width:92%;" > 
								 
							</fieldset>
								<div class="clear"></div>
								<fieldset style="width:48%; float:left;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Rental Extra Price</label>
								<input type="text" name="rental_extraprice" value="<?php echo $row['rental_extraprice']; ?>" style="width:92%;" >
							</fieldset>
							<fieldset style="width:48%; float:right;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Rental model </label>
								<input type="text" name="rental_model" value="<?php echo $row['rental_extraprice']; ?>" style="width:92%;">
							</fieldset>
							<!--<fieldset style="width:48%; float:right;"> 
								<label>Rental Model Chinese </label>
								<input type="text" name="rental_model_zh" value="" style="width:92%;" > 
							</fieldset>-->
								<div class="clear"></div>
							

							<fieldset style="width:48%; float:left; margin-right: 3%;">
								<label>Rental Features</label>
								<textarea rows="5" name="rental_features" id="content" style="width:92%;"><?php echo $row['rental_features']; ?></textarea>
								<!--<label>Rental Features Chinese</label>
								<textarea rows="5" name="rental_features_zh" id="content" style="width:92%;"> </textarea>-->
							</fieldset>

							<fieldset style="width:48%; float:right;"> 
								<label>Rental Deal</label>
								<textarea rows="5" name="rental_deal" id="content"style="width:92%;"><?php echo $row['rental_deal']; ?></textarea>
								<!--<label>rental Deal Chinese</label>
								<textarea rows="5" name="rental_deal_zh" id="content" style="width:92%;"></textarea>-->
							</fieldset>

							<fieldset style="width:48%; float:left; "> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Video URL </label>
								<div style="width:100%;  float:left; overflow:hidden ;">
									<input type="text" name = "rental_video" style="width:70%;  float:left; margin-right: 3%;" value="<?php echo $row['rental_extraprice']; ?>">
								</div>
							</fieldset>
							
							<fieldset style="width:48%; float:right; "> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Booking Form URL </label>
								<input type="text" name = "rental_bookingform" value="<?php echo $row['rental_bookingform']; ?>" style="width:96%;">
								<!--<label>Booking Form URL Chinese</label>
								<input type="text" name = "rental_booking_form_zh" value="" style="width:96%;">-->
							</fieldset>
							<fieldset style="width:48%; float:left; "> <!-- to make two field float next to one another, adjust values accordingly -->
							<label>Rental Company</label>
								<select style="width:92%;" name="rcompany" id="rcompany" >
								<?php 
								$resultq = $conn->query("SELECT * FROM `rcompany`"); 

								while($row = $resultq->fetch_assoc()){ 						
	    						
	    								echo "<option value=\"" .$row['rcompany_id']. "\">" . $row['rcompany_name'] ."</option>" ;
								} 
									
								?>
								</select>
							</fieldset>

							
								
								<fieldset style="width:48%; float:right; "> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Lanuage Type</label>
									<select style="width:92%;" name="language" id="language" >
									<?php 
									$resultq = $conn->query("SELECT * FROM `language`where lang_id='$varLang'"); 

									while($rowq = $resultq->fetch_assoc()){ 						
		    						
		    								echo "<option value=\"" .$rowq['lang_id']. "\">" . $rowq['lang_name'] ."</option>" ;
									} 
										
									?>
									</select>
								</fieldset>
							
							<fieldset style="width:48%; float:left; "> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Rental Logo</label>
								<br>
								<input type="file" name="logo" style=" float:left; margin-right: 3%;" >
							</fieldset>
								
								<div class="clear"></div>
								<fieldset><label for="chkImage">
									<input type="checkbox" name="chkImage" id="chkImage" value="yes" onclick="ShowHideDiv(this)" />
									    Select to Update Image
									</label>
								</fieldset>

								<div id="dvPassport" style="display: none">
								<fieldset>
										
									    <script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
										    <div class="file-upload">

										      <button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Update Image</button>

										      <div class="image-upload-wrap">
										        <input class="file-upload-input" type='file' name="UploadImage" id = "UploadImage" onchange="readURL(this);" accept="image/*" />
										        <div class="drag-text">
										          <h3>Drag and drop a file or select add Image to Update
										          	
										          	<img src="<?php echo $row['rental_logo']; ?>" alt= "" style= "height:300px;width:300px">
										          
										          </h3>
										        </div>
										      </div>
										      <div class="file-upload-content">
										        <img class="file-upload-image" src="#" alt="your image" />
										        <div class="image-title-wrap">
										          <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
										        </div>
										      </div>
										    </div>
    
 										 <script src="js/index.js"></script>
									
								</fieldset>
							</div>
								<footer>
						<div class="submit_link">
							<input type="submit" value="Update" class="alt_btn">
						</div>
						</footer>
					
					</form>
				</div>



				<!-- module extra tab -->
				<div id="tab2" class="tab_content">
					<header><h3>Chinese</h3></header>
					<form action="editrentals.php?id=<?php echo $id;?>&lang=<?php echo "2";?>" method="post" enctype="multipart/form-data">
						<?php 			
						$varLang=2;
						$result = $conn->query("SELECT * FROM rentals WHERE rentalmulti_id = '$id' and language_lang_id='2'"); 

							$row = $result->fetch_assoc();
							
						 ?>
						<fieldset style="width:48%; float:left; margin-right: 3%;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>rental Name </label>
								<input type="text" name="rental_name" value="<?php echo $row['rental_name']; ?>" style="width:92%;" > 
							</fieldset>
														<!-- Another Row -->
							<fieldset style="width:48%; float:right;">
								<label>Rental Price</label>
								<input type="text" name="rental_price" value="<?php echo $row['rental_price']; ?>" style="width:92%;" > 
								 
							</fieldset>
								<div class="clear"></div>
								<fieldset style="width:48%; float:left;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Rental Extra Price</label>
								<input type="text" name="rental_extraprice" value="<?php echo $row['rental_extraprice']; ?>" style="width:92%;" >
							</fieldset>
							<fieldset style="width:48%; float:right;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Rental model </label>
								<input type="text" name="rental_model" value="<?php echo $row['rental_extraprice']; ?>" style="width:92%;">
							</fieldset>
							<!--<fieldset style="width:48%; float:right;"> 
								<label>Rental Model Chinese </label>
								<input type="text" name="rental_model_zh" value="" style="width:92%;" > 
							</fieldset>-->
								<div class="clear"></div>
							

							<fieldset style="width:48%; float:left; margin-right: 3%;">
								<label>Rental Features</label>
								<textarea rows="5" name="rental_features" id="content" style="width:92%;"><?php echo $row['rental_features']; ?></textarea>
								<!--<label>Rental Features Chinese</label>
								<textarea rows="5" name="rental_features_zh" id="content" style="width:92%;"> </textarea>-->
							</fieldset>

							<fieldset style="width:48%; float:right;"> 
								<label>Rental Deal</label>
								<textarea rows="5" name="rental_deal" id="content"style="width:92%;"><?php echo $row['rental_deal']; ?></textarea>
								<!--<label>rental Deal Chinese</label>
								<textarea rows="5" name="rental_deal_zh" id="content" style="width:92%;"></textarea>-->
							</fieldset>

							<fieldset style="width:48%; float:left; "> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Video URL </label>
								<div style="width:100%;  float:left; overflow:hidden ;">
									<input type="text" name = "rental_video" style="width:70%;  float:left; margin-right: 3%;" value="<?php echo $row['rental_extraprice']; ?>">
								</div>
							</fieldset>
							
							<fieldset style="width:48%; float:right; "> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Booking Form URL </label>
								<input type="text" name = "rental_bookingform" value="<?php echo $row['rental_bookingform']; ?>" style="width:96%;">
								<!--<label>Booking Form URL Chinese</label>
								<input type="text" name = "rental_booking_form_zh" value="" style="width:96%;">-->
							</fieldset>
							<fieldset style="width:48%; float:left; "> <!-- to make two field float next to one another, adjust values accordingly -->
							<label>Rental Company</label>
								<select style="width:92%;" name="rcompany" id="rcompany" >
								<?php 
								$resultq = $conn->query("SELECT `rcompany_name` from `rcompany` inner join `rentals` on rcompany.rcompany_name = rentals.rcompany_id WHERE rentalmulti_id = $id;"); 

								while($row = $resultq->fetch_assoc()){ 						
	    						
	    								echo "<option value=\"" .$row['rcompany_id']. "\">" . $row['rcompany_name'] ."</option>" ;
								} 
									
								?>
								</select>
							</fieldset>

							
								
								<fieldset style="width:48%; float:right; "> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Lanuage Type</label>
									<select style="width:92%;" name="language" id="language" >
									<?php 
									$resultq = $conn->query("SELECT * FROM `language`where lang_id='$varLang'"); 

									while($rowq = $resultq->fetch_assoc()){ 						
		    						
		    								echo "<option value=\"" .$rowq['lang_id']. "\">" . $rowq['lang_name'] ."</option>" ;
									} 
										
									?>
									</select>
								</fieldset>
								<div class="clear"></div>
								<!-- <fieldset><label for="chkImage">
									<input type="checkbox" name="chkImage" id="chkImage" value="yes" onclick="ShowHideDivChin(this)" />
									    Select to Update Image
									</label>
								</fieldset> -->
								<!-- <div id="dvChin" style="display: none">
									<fieldset>
											
										    <script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
											    <div class="file-upload">

											      <button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Add Image</button>

											      <div class="image-upload-wrap">
											        <input class="file-upload-input" type='file' name="UploadImage" id = "UploadImage" onchange="readURL(this);" accept="image/*" />
											        <div class="drag-text">
											          <h3>Drag and drop a file or select add Image to Update
											          	<img src="<?php echo $row['tour_image'];?> " alt= "" style= "height:300px;">
											          </h3>
											        </div>
											      </div>
											      <div class="file-upload-content">
											        <img class="file-upload-image" src="#" alt="your image" />
											        <div class="image-title-wrap">
											          <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
											        </div>
											      </div>
											    </div>
	    
	 										 <script src="js/index.js"></script>
										
									</fieldset>
								</div> -->
								<footer>
						<div class="submit_link">
							<input type="submit" value="Update" class="alt_btn">
						</div>
						</footer>

					</form>
				</div>

				<div id="tab3" class="tab_content">
					<br/>
					<header><h3>Japanese</h3></header>
					<form action="editrentals.php?id=<?php echo $id;?>&lang=<?php echo "3";?>" method="post" enctype="multipart/form-data">
						<?php 			
						$varLang=3;
						$result = $conn->query("SELECT * FROM rentals WHERE rentalmulti_id = '$id' and language_lang_id='3'"); 

							$row = $result->fetch_assoc();
							
						 ?>
						 <fieldset style="width:48%; float:left; margin-right: 3%;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>rental Name </label>
								<input type="text" name="rental_name" value="<?php echo $row['rental_name']; ?>" style="width:92%;" > 
							</fieldset>
														<!-- Another Row -->
							<fieldset style="width:48%; float:right;">
								<label>Rental Price</label>
								<input type="text" name="rental_price" value="<?php echo $row['rental_price']; ?>" style="width:92%;" > 
								 
							</fieldset>
								<div class="clear"></div>
								<fieldset style="width:48%; float:left;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Rental Extra Price</label>
								<input type="text" name="rental_extraprice" value="<?php echo $row['rental_extraprice']; ?>" style="width:92%;" >
							</fieldset>
							<fieldset style="width:48%; float:right;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Rental model </label>
								<input type="text" name="rental_model" value="<?php echo $row['rental_extraprice']; ?>" style="width:92%;">
							</fieldset>
							<!--<fieldset style="width:48%; float:right;"> 
								<label>Rental Model Chinese </label>
								<input type="text" name="rental_model_zh" value="" style="width:92%;" > 
							</fieldset>-->
								<div class="clear"></div>
							

							<fieldset style="width:48%; float:left; margin-right: 3%;">
								<label>Rental Features</label>
								<textarea rows="5" name="rental_features" id="content" style="width:92%;"><?php echo $row['rental_features']; ?></textarea>
								<!--<label>Rental Features Chinese</label>
								<textarea rows="5" name="rental_features_zh" id="content" style="width:92%;"> </textarea>-->
							</fieldset>

							<fieldset style="width:48%; float:right;"> 
								<label>Rental Deal</label>
								<textarea rows="5" name="rental_deal" id="content"style="width:92%;"><?php echo $row['rental_deal']; ?></textarea>
								<!--<label>rental Deal Chinese</label>
								<textarea rows="5" name="rental_deal_zh" id="content" style="width:92%;"></textarea>-->
							</fieldset>

							<fieldset style="width:48%; float:left; "> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Video URL </label>
								<div style="width:100%;  float:left; overflow:hidden ;">
									<input type="text" name = "rental_video" style="width:70%;  float:left; margin-right: 3%;" value="<?php echo $row['rental_extraprice']; ?>">
								</div>
							</fieldset>
							
							<fieldset style="width:48%; float:right; "> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Booking Form URL </label>
								<input type="text" name = "rental_bookingform" value="<?php echo $row['rental_bookingform']; ?>" style="width:96%;">
								<!--<label>Booking Form URL Chinese</label>
								<input type="text" name = "rental_booking_form_zh" value="" style="width:96%;">-->
							</fieldset>
							<fieldset style="width:48%; float:left; "> <!-- to make two field float next to one another, adjust values accordingly -->
							<label>Rental Company</label>
								<select style="width:92%;" name="rcompany" id="rcompany" >
								<?php 
								$resultq = $conn->query("SELECT * FROM `rcompany`"); 

								while($row = $resultq->fetch_assoc()){ 						
	    						
	    								echo "<option value=\"" .$row['rcompany_id']. "\">" . $row['rcompany_name'] ."</option>" ;
								} 
									
								?>
								</select>
							</fieldset>

							
								
								<fieldset style="width:48%; float:right; "> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Lanuage Type</label>
									<select style="width:92%;" name="language" id="language" >
									<?php 
									$resultq = $conn->query("SELECT * FROM `language`where lang_id='$varLang'"); 

									while($rowq = $resultq->fetch_assoc()){ 						
		    						
		    								echo "<option value=\"" .$rowq['lang_id']. "\">" . $rowq['lang_name'] ."</option>" ;
									} 
										
									?>
									</select>
								</fieldset>

								<div class="clear"></div>
								<!-- <fieldset><label for="chkImage">
									<input type="checkbox" name="chkImage" id="chkImage" value="yes" onclick="ShowHideDivJap(this)" />
									    Select to Update Image
									</label>
								</fieldset> -->
								<!-- <div id="dvJap" style="display: none">
								<fieldset>
										
									    <script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
										    <div class="file-upload">

										      <button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Add Image</button>

										      <div class="image-upload-wrap">
										        <input class="file-upload-input" type='file' name="UploadImage" id = "UploadImage" onchange="readURL(this);" accept="image/*" />
										        <div class="drag-text">
										          <h3>Drag and drop a file or select add Image to Update
										          	<img src="<?php echo $row['tour_image'];?> " alt= "" style= "height:300px;">
										          </h3>
										        </div>
										      </div>
										      <div class="file-upload-content">
										        <img class="file-upload-image" src="#" alt="your image" />
										        <div class="image-title-wrap">
										          <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
										        </div>
										      </div>
										    </div>
    
 										 <script src="js/index.js"></script>
									
								</fieldset>
								
							</div> -->

								<footer>
						<div class="submit_link">
							<input type="submit" value="Update" class="alt_btn">
						</div>
						</footer>

					</form>
				</div>


		</div>



		<div class="clear"></div>

	</div>


</article><!-- end of post new article -->

<div class="spacer"></div>
</section>
</body>

</html>



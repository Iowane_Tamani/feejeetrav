<?php 
session_start();

include_once"config.php";
if(!isset($_SESSION['username']) || !isset($_SESSION['password'])){
	header("Location: login.php");
}else{
//$fetch_users_data = mysql_fetch_object(mysql_query("SELECT * FROM `members` WHERE username='".$_SESSION['username']."'"));
}
?>
<?php
header('Content-Type: text/html; charset=utf8_general_ci');
$id = $_GET['id'];

mysqli_set_charset($conn,"utf8");
$result = $conn->query("SELECT * FROM tours WHERE tourmulti_id = '$id' AND tour_listingtype = 1"); 

?>

<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8"/>

	<title>Edit Tour</title>
	
	<link rel="stylesheet" href="css/layout.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="css/style.css">
	<!--[if lt IE 9]>
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script src="js/jquery-1.5.2.min.js" type="text/javascript"></script>
<script src="js/hideshow.js" type="text/javascript"></script>
<script src="js/jquery.tablesorter.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery.equalHeight.js"></script>
<script type="text/javascript">
	$(document).ready(function() 
	{ 
		$(".tablesorter").tablesorter(); 
	} 
	);
	$(document).ready(function() {

	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});
</script>

<script type="text/javascript">
	$(function(){
		$('.column').equalHeight();
	});
</script>
<!-- Function for Showing FileUpload option -->
<script type="text/javascript">
	function ShowHideDiv(chkImage) {
		var dvPassport = document.getElementById("dvPassport");
		dvPassport.style.display = chkImage.checked ? "block" : "none";
	}

	function ShowHideDivChin(chkImage) {
			var dvChin = document.getElementById("dvChin");
		dvChin.style.display = chkImage.checked ? "block" : "none";

	}

	function ShowHideDivJap(chkImage) {
			var dvJap = document.getElementById("dvJap");
		dvJap.style.display = chkImage.checked ? "block" : "none";
	}
</script>
</head>


<body>

	<header id="header">
		<hgroup>
			<h1 class="site_title"><a href="index.php">Feejee Traveller</a></h1>
			<h2 class="section_title">Edit Tours </h2>
		</hgroup>
	</header> <!-- end of header bar -->
	
	<section id="secondary_bar">
		<div class="user">
			<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
		</div>
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="index.php">Feejee Traveller</a> <div class="breadcrumb_divider"></div> <a class="current">Tour</a></article>
		</div>
	</section><!-- end of secondary bar -->
	
	<?php include('navBar.php'); ?>
	
	<section id="main" class="column">

		<div class="clear"></div>
		
		<article class="module width_full">
			<header><h3 class="tabs_involved">Tour</h3>
				<ul class="tabs">
					<li><a href="#tab1">English</a></li>
					<li><a href="#tab2">Chinese</a></li>
					<li><a href="#tab3">Japanese</a></li>

				</ul>
			</header>
			
			
		<div class="tab_container">
				<div id="tab1" class="tab_content">
				<header><h3>English</h3></header>
					<form action="editTour.php?id=<?php echo $id;?>&lang=<?php echo "1";?>" method="post" enctype="multipart/form-data">
						<?php 		
							$varLang=1;
							$result = $conn->query("SELECT * FROM tours WHERE tourmulti_id = '$id' AND tour_listingtype = '1' and language_lang_id='1'"); 
	
							$row = $result->fetch_assoc();
							$categoryID = $row['tour_category_tcatmulti_id'];
						 ?>

						 <fieldset style="width:48%; float:left; margin-right: 3%;"> <!-- to make two field float next to one another, adjust values accordingly -->
									<label>Tour Name </label>
									<input type="text" name="tour_name" value="<?php echo $row['tour_name']; ?>" style="width:92%;" > 
								</fieldset>
								<fieldset style="width:48%; float:right;"> <!-- to make two field float next to one another, adjust values accordingly -->
									<label>Trip Advisory </label>
									<input type="text" name="trip" value="<?php echo $row['tripadvisorid']; ?>" style="width:92%;" > 
								</fieldset>
								<!-- Another Row -->
								<fieldset style="width:48%; float:left;">
									<label>Tour Description</label>
									<textarea rows="5" name="tour_description" id="content" style="width:92%;"><?php echo $row['tour_description']; ?></textarea>
									
								</fieldset>
								<fieldset style="width:48%; float:right;"> 
									<label>Tour Highlights</label>
									<textarea rows="5" name="tour_highlight" id="content"style="width:92%;"><?php echo $row['tour_highlight']; ?></textarea>
									<!--<label>Tour Highlights Chinese</label>
									<textarea rows="5" name="tour_highlight_zh" id="content" style="width:92%;"></textarea>l-->
								</fieldset>
								<!-- Another Row -->
								<fieldset style="width:48%; float:left;">
									<label>Tour Duration</label>
									<textarea rows="5" name="tour_duration" id="content" style="width:92%;"><?php echo $row['tour_duration']; ?></textarea>
									
								</fieldset>
								<fieldset style="width:48%; float:right;"> 
									<label>Tour Guide</label>
									<textarea rows="5" name="tour_guide" id="content" style="width:92%;"><?php echo $row['tour_guide']; ?></textarea>
									
								</fieldset>
								<fieldset style="width:48%; float:left; margin-right: 3%;"> <!-- to make two field float next to one another, adjust values accordingly -->
									<label>Form URL </label>
									<input type="text" name="tour_formurl" value="<?php echo $row['tour_formurl']; ?>" style="width:92%;" > 
								</fieldset>
								<fieldset style="width:48%; float:right; "> <!-- to make two field float next to one another, adjust values accordingly -->
									<label>Video URL</label>
									<input type="text" name="tour_video" value="<?php echo $row['tour_video']; ?>" style="width:92%;" > 
								</fieldset>
								<fieldset style="width:48%; float:left; margin-right: 3%;"> <!-- to make two field float next to one another, adjust values accordingly -->
									<label>Category</label>

								<select style="width:92%;" name="category" id="category" >
									<?php 
									$resultq = $conn->query("SELECT * FROM `tour_category`"); 

									while($rowq = $resultq->fetch_assoc()){ 
										if ((strcmp($row['id'],$categoryID) == 0)) {
											echo $rowq['id'] . $rowq['tcat_name'];
			    								echo "<option value=\"" .$rowq['id']. "\"selected>" . $rowq['tcat_name'] ."</option>" ;
										}else{

			    							echo $rowq['id'] . $rowq['tcat_name'];
			    								echo "<option value=\"" .$rowq['id']. "\">" . $rowq['tcat_name'] ."</option>" ;
			    						}
									} 
										
									?>
									</select>
									</fieldset>
								<fieldset style="width:48%; float:right; "> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Lanuage Type</label>
									<select style="width:92%;" name="language" id="language" >
									<?php 
									$resultq = $conn->query("SELECT * FROM `language`where lang_id='$varLang'"); 

									while($rowq = $resultq->fetch_assoc()){ 						
		    						
		    								echo "<option value=\"" .$rowq['lang_id']. "\">" . $rowq['lang_name'] ."</option>" ;
									} 
										
									?>
									</select>
								</fieldset>

								<div class="clear"></div>
								<fieldset><label for="chkImage">
									<input type="checkbox" name="chkImage" id="chkImage" value="yes" onclick="ShowHideDiv(this)" />
									    Select to Update Image
									</label>
								</fieldset>

								<div id="dvPassport" style="display: none">
								<fieldset>
										
									    <script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
										    <div class="file-upload">

										      <button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Add Image</button>

										      <div class="image-upload-wrap">
										        <input class="file-upload-input" type='file' name="UploadImage" id = "UploadImage" onchange="readURL(this);" accept="image/*" />
										        <div class="drag-text">
										          <h3>Drag and drop a file or select add Image to Update
										          	<img src="<?php echo $row['tour_image'];?> " alt= "" style= "height:300px;width:300px">
										          
										          </h3>
										        </div>
										      </div>
										      <div class="file-upload-content">
										        <img class="file-upload-image" src="#" alt="your image" />
										        <div class="image-title-wrap">
										          <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
										        </div>
										      </div>
										    </div>
    
 										 <script src="js/index.js"></script>
									
								</fieldset>
							</div>
								<footer>
						<div class="submit_link">
							<input type="submit" value="Update" class="alt_btn">
						</div>
						</footer>
					
					</form>
				</div>



				<!-- module extra tab -->
				<div id="tab2" class="tab_content">
					<header><h3>Chinese</h3></header>
					<form action="editTour.php?id=<?php echo $id;?>&lang=<?php echo "2";?>" method="post" enctype="multipart/form-data">
						<?php 			
						$varLang=2;
						$result = $conn->query("SELECT * FROM tours WHERE tourmulti_id = '$id' AND tour_listingtype = '1' and language_lang_id='2'"); 

							$row = $result->fetch_assoc();
							$categoryID = $row['tour_category_tcatmulti_id'];
						 ?>
						 <fieldset style="width:48%; float:left; margin-right: 3%;"> <!-- to make two field float next to one another, adjust values accordingly -->
									<label>Tour Name </label>
									<input type="text" name="tour_name" value="<?php echo $row['tour_name']; ?>" style="width:92%;" > 
								</fieldset>
								<fieldset style="width:48%; float:right;"> <!-- to make two field float next to one another, adjust values accordingly -->
									<label>Trip Advisory </label>
									<input type="text" name="trip" value="<?php echo $row['tripadvisorid']; ?>" style="width:92%;" > 
								</fieldset>
								<!-- Another Row -->
								<fieldset style="width:48%; float:left;">
									<label>Tour Description</label>
									<textarea rows="5" name="tour_description" id="content" style="width:92%;"><?php echo $row['tour_description']; ?></textarea>
									
								</fieldset>
								<fieldset style="width:48%; float:right;"> 
									<label>Tour Highlights</label>
									<textarea rows="5" name="tour_highlight" id="content"style="width:92%;"><?php echo $row['tour_highlight']; ?></textarea>
									<!--<label>Tour Highlights Chinese</label>
									<textarea rows="5" name="tour_highlight_zh" id="content" style="width:92%;"></textarea>l-->
								</fieldset>
								<!-- Another Row -->
								<fieldset style="width:48%; float:left;">
									<label>Tour Duration</label>
									<textarea rows="5" name="tour_duration" id="content" style="width:92%;"><?php echo $row['tour_duration']; ?></textarea>
									
								</fieldset>
								<fieldset style="width:48%; float:right;"> 
									<label>Tour Guide</label>
									<textarea rows="5" name="tour_guide" id="content" style="width:92%;"><?php echo $row['tour_guide']; ?></textarea>
									
								</fieldset>
								<fieldset style="width:48%; float:left; margin-right: 3%;"> <!-- to make two field float next to one another, adjust values accordingly -->
									<label>Form URL </label>
									<input type="text" name="tour_formurl" value="<?php echo $row['tour_formurl']; ?>" style="width:92%;" > 
								</fieldset>
								<fieldset style="width:48%; float:right; "> <!-- to make two field float next to one another, adjust values accordingly -->
									<label>Video URL</label>
									<input type="text" name="tour_video" value="<?php echo $row['tour_video']; ?>" style="width:92%;" > 
								</fieldset>
								<fieldset style="width:48%; float:left; margin-right: 3%;"> <!-- to make two field float next to one another, adjust values accordingly -->
									<label>Category</label>

								<select style="width:92%;" name="category" id="category" >
									<?php 
									$resultq = $conn->query("SELECT * FROM `tour_category`"); 

									while($row = $resultq->fetch_assoc()){ 
										if ((strcmp($row['id'],$categoryID) == 0)) {
											echo $row['id'] . $row['tcat_name'];
			    								echo "<option value=\"" .$row['id']. "\"selected>" . $row['tcat_name'] ."</option>" ;
										}else{

			    							echo $row['id'] . $row['tcat_name'];
			    								echo "<option value=\"" .$row['id']. "\">" . $row['tcat_name'] ."</option>" ;
			    						}
									} 
										
									?>
									</select>
									</fieldset>
								<fieldset style="width:48%; float:right; "> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Lanuage Type</label>
									<select style="width:92%;" name="language" id="language" >
									<?php 
									$resultq = $conn->query("SELECT * FROM `language` where lang_id='$varLang'"); 

									while($row = $resultq->fetch_assoc()){ 						
		    						
		    								echo "<option value=\"" .$row['lang_id']. "\">" . $row['lang_name'] ."</option>" ;
									} 
										
									?>
									</select>
								</fieldset>

								<div class="clear"></div>
								<!-- <fieldset><label for="chkImage">
									<input type="checkbox" name="chkImage" id="chkImage" value="yes" onclick="ShowHideDivChin(this)" />
									    Select to Update Image
									</label>
								</fieldset> -->
								<!-- <div id="dvChin" style="display: none">
									<fieldset>
											
										    <script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
											    <div class="file-upload">

											      <button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Add Image</button>

											      <div class="image-upload-wrap">
											        <input class="file-upload-input" type='file' name="UploadImage" id = "UploadImage" onchange="readURL(this);" accept="image/*" />
											        <div class="drag-text">
											          <h3>Drag and drop a file or select add Image to Update
											          	<img src="<?php echo $row['tour_image'];?> " alt= "" style= "height:300px;">
											          </h3>
											        </div>
											      </div>
											      <div class="file-upload-content">
											        <img class="file-upload-image" src="#" alt="your image" />
											        <div class="image-title-wrap">
											          <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
											        </div>
											      </div>
											    </div>
	    
	 										 <script src="js/index.js"></script>
										
									</fieldset>
								</div> -->
								<footer>
						<div class="submit_link">
							<input type="submit" value="Update" class="alt_btn">
						</div>
						</footer>

					</form>
				</div>

				<div id="tab3" class="tab_content">
					<br/>
					<header><h3>Japanese</h3></header>
					<form action="editTour.php?id=<?php echo $id;?>&lang=<?php echo "3";?>" method="post" enctype="multipart/form-data">
						<?php 			
						$varLang=3;
						$result = $conn->query("SELECT * FROM tours WHERE tourmulti_id = '$id' AND tour_listingtype = '1' and language_lang_id='3'"); 

							$row = $result->fetch_assoc();
							$categoryID = $row['tour_category_tcatmulti_id'];
						 ?>
						 <fieldset style="width:48%; float:left; margin-right: 3%;"> <!-- to make two field float next to one another, adjust values accordingly -->
									<label>Tour Name </label>
									<input type="text" name="tour_name" value="<?php echo $row['tour_name']; ?>" style="width:92%;" > 
								</fieldset>
								<fieldset style="width:48%; float:right;"> <!-- to make two field float next to one another, adjust values accordingly -->
									<label>Trip Advisory </label>
									<input type="text" name="trip" value="<?php echo $row['tripadvisorid']; ?>" style="width:92%;" > 
								</fieldset>
								<!-- Another Row -->
								<fieldset style="width:48%; float:left;">
									<label>Tour Description</label>
									<textarea rows="5" name="tour_description" id="content" style="width:92%;"><?php echo $row['tour_description']; ?></textarea>
									
								</fieldset>
								<fieldset style="width:48%; float:right;"> 
									<label>Tour Highlights</label>
									<textarea rows="5" name="tour_highlight" id="content"style="width:92%;"><?php echo $row['tour_highlight']; ?></textarea>
									<!--<label>Tour Highlights Chinese</label>
									<textarea rows="5" name="tour_highlight_zh" id="content" style="width:92%;"></textarea>l-->
								</fieldset>
								<!-- Another Row -->
								<fieldset style="width:48%; float:left;">
									<label>Tour Duration</label>
									<textarea rows="5" name="tour_duration" id="content" style="width:92%;"><?php echo $row['tour_duration']; ?></textarea>
									
								</fieldset>
								<fieldset style="width:48%; float:right;"> 
									<label>Tour Guide</label>
									<textarea rows="5" name="tour_guide" id="content" style="width:92%;"><?php echo $row['tour_guide']; ?></textarea>
									
								</fieldset>
								<fieldset style="width:48%; float:left; margin-right: 3%;"> <!-- to make two field float next to one another, adjust values accordingly -->
									<label>Form URL </label>
									<input type="text" name="tour_formurl" value="<?php echo $row['tour_formurl']; ?>" style="width:92%;" > 
								</fieldset>
								<fieldset style="width:48%; float:right; "> <!-- to make two field float next to one another, adjust values accordingly -->
									<label>Video URL</label>
									<input type="text" name="tour_video" value="<?php echo $row['tour_video']; ?>" style="width:92%;" > 
								</fieldset>
								<fieldset style="width:48%; float:left; margin-right: 3%;"> <!-- to make two field float next to one another, adjust values accordingly -->
									<label>Category</label>

								<select style="width:92%;" name="category" id="category" >
									<?php 
									$resultq = $conn->query("SELECT * FROM `tour_category`"); 

									while($row = $resultq->fetch_assoc()){ 
										if ((strcmp($row['id'],$categoryID) == 0)) {
											echo $row['id'] . $row['tcat_name'];
			    								echo "<option value=\"" .$row['id']. "\"selected>" . $row['tcat_name'] ."</option>" ;
										}else{

			    							echo $row['id'] . $row['tcat_name'];
			    								echo "<option value=\"" .$row['id']. "\">" . $row['tcat_name'] ."</option>" ;
			    						}
									} 
										
									?>
									</select>
									</fieldset>
								<fieldset style="width:48%; float:right; "> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Language Type</label>
									<select style="width:92%;" name="language" id="language" >
									<?php 
									$resultq = $conn->query("SELECT * FROM `language` where lang_id='$varLang'"); 

									while($row = $resultq->fetch_assoc()){ 						
		    						
		    								echo "<option value=\"" .$row['lang_id']. "\">" . $row['lang_name'] ."</option>" ;
									} 
										
									?>
									</select>
								</fieldset>

								<div class="clear"></div>
								<!-- <fieldset><label for="chkImage">
									<input type="checkbox" name="chkImage" id="chkImage" value="yes" onclick="ShowHideDivJap(this)" />
									    Select to Update Image
									</label>
								</fieldset> -->
								<!-- <div id="dvJap" style="display: none">
								<fieldset>
										
									    <script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
										    <div class="file-upload">

										      <button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Add Image</button>

										      <div class="image-upload-wrap">
										        <input class="file-upload-input" type='file' name="UploadImage" id = "UploadImage" onchange="readURL(this);" accept="image/*" />
										        <div class="drag-text">
										          <h3>Drag and drop a file or select add Image to Update
										          	<img src="<?php echo $row['tour_image'];?> " alt= "" style= "height:300px;">
										          </h3>
										        </div>
										      </div>
										      <div class="file-upload-content">
										        <img class="file-upload-image" src="#" alt="your image" />
										        <div class="image-title-wrap">
										          <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
										        </div>
										      </div>
										    </div>
    
 										 <script src="js/index.js"></script>
									
								</fieldset>
								
							</div> -->

								<footer>
						<div class="submit_link">
							<input type="submit" value="Update" class="alt_btn">
						</div>
						</footer>

					</form>
				</div>


		</div>



		<div class="clear"></div>

	</div>


</article><!-- end of post new article -->

<div class="spacer"></div>
</section>
</body>

</html>



<?php 
	//Connect to database
	$servername = "localhost";
	$username = "root";
	$password = "root";
	$dbname = "newswebAir";
		// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		} 
	date_default_timezone_set("Pacific/Fiji");
	// Storing the form fields to variables
	$title =$_POST["title"];
	$content =$_POST["content"];
	$categoryId = $_POST["category"];
	$tags = $_POST["tags"];
	$check = $_POST['chkImage'];
	$Likes = 0;
	$Read= 0;
	// Setting some default dummy variables
	$author = "John Doe";
	$trimContent ="Blah blah blah";
	$Photographer = "Peter Parker";
	$tagsId = 1;
	$DateCreated = date('Y-m-d H:i:s');
	$content = preg_replace("#[^A-Za-z0-9. \n]#i", "", $content);
	// Image upload to server and Adding URL to databse
	if(isset($_FILES['UploadImage'])){
		// Creates the Variables needed to upload the file
		$UploadName = $_FILES['UploadImage']['name'];
		$UploadName = mt_rand(100, 999).$UploadName;
		$UploadTmp = $_FILES['UploadImage']['tmp_name'];
		$UploadType = $_FILES['UploadImage']['type'];
		$FileSize = $_FILES['UploadImage']['size'];
		list($width, $height) = getimagesize($UploadTmp);
		// Removes Unwanted Spaces and characters from the files names of the files being uploaded
		$UploadName = preg_replace("#[^a-z0-9.]#i", "", $UploadName);
		// Upload File Size Limit 
		if(($FileSize > 3450000)){	
			die("Error - File to Big");
		}
		// Checks a File has been Selected and Uploads them into a Directory on your Server
		if(!$UploadTmp && (strcmp($check,'yes') ==0)){
			die("No File Selected, Please Upload Again");
		}else if (strcmp($check,'yes') !==0 || !$UploadTmp){
			$sql = "INSERT INTO Article (Title, Author, StoryBrief, StoryFull, categoryId, tagsId, DateCreated)
			VALUES ('$title','$author','$trimContent','$content', '$categoryId', '$tagsId', '$DateCreated')";
			if ($conn->query($sql) == TRUE) {
			} else {
	    		//echo "Error: " . $sql . "<br>" . $conn->error;
			}
		}
		else{
			move_uploaded_file($UploadTmp, "gallery/$UploadName");
			$url = "http://192.168.10.127:8888/ProjectSpideyWeb/gallery/$UploadName";
			$sql = "INSERT INTO Article (Title, Author, StoryBrief, StoryFull, ImageURL,Photographer,categoryId, tagsId, DateCreated, Likes, numRead)
			VALUES ('$title','$author','$trimContent','$content', '$url', '$Photographer', '$categoryId', '$tagsId', '$DateCreated', $Likes, $Read)";

			if ($conn->query($sql) == TRUE) {
			} else {
	    		//echo "Error: " . $sql . "<br>" . $conn->error;
			}
		}
	}
	// Close connection to the database
	$conn->close();
	header("Location: newArticle.php");
	exit();
?>

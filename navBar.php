<aside id="sidebar" class="column">
		
		<hr/>
		<h3>AIRLINES</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="newairline.php">Add New Airline</a></li>
			<li class="icn_categories"><a href="viewairline.php">View Airline</a></li>
		<h3>Home Featured</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="newhome.php">Add Home Featured</a></li>
			<li class="icn_categories"><a href="viewairline.php">View Home Featured</a></li>
		<h3>TOURS</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="newTour.php">Add New Tour</a></li>
			<li class="icn_new_article"><a href="Tourlang.php">Add Tour Language</a></li>
			<li class="icn_categories"><a href="viewTours.php">View Tours</a></li>
			<li class="icn_settings"><a href="tourrates.php">Tour Rates</a></li>
		</ul>
		<h3>EVENTS</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="newevent.php">Add New Events</a></li>
			<li class="icn_new_article"><a href="eventlang.php">Add Event Language</a></li>
			<li class="icn_categories"><a href="viewevents.php">View Events</a></li>
		</ul>
		<h3>Business</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="newbusiness.php">Add New Business</a></li>
			<li class="icn_new_article"><a href="buslang.php">Add Business Language</a></li>
			<li class="icn_categories"><a href="viewbusinesses.php">View Businesses</a></li>
			<li class="icn_new_article"><a href="newbusvc.php">Add New Business Voucher</a></li>
			<li class="icn_new_article"><a href="busvclang.php">Add Language Business Voucher</a></li>
			<li class="icn_new_article"><a href="viewbusvc.php">View Business Voucher</a></li>
		</ul>
		<h3>Rentals</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="newrental.php">Add New Rental Package</a></li>
			<li class="icn_new_article"><a href="rentallang.php">Add Rental Package Language</a></li>
			<li class="icn_categories"><a href="viewrentals.php">View Rental Package</a></li>
 		</ul>
		<h3>Rental Companies</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="newrcomp.php">Add Rental</a></li>
			<li class="icn_categories"><a href="viewrcomp.php">View Rental</a></li>
		</ul>
		<h3>Currency</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="newcurrency.php">Update Currency</a></li>
		</ul>
		<h3>Etiquette</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="newet.php">Add New Etiquette</a></li>
			<li class="icn_new_article"><a href="etlang.php">Add Etiquette Language</a></li>
			<li class="icn_categories"><a href="viewet.php">View Etiquette</a></li>
		</ul>
		<h3>App Settings</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="newapp.php">Add New App Settings</a></li>
			<li class="icn_new_article"><a href="applang.php">Add App Settings Language</a></li>
			<li class="icn_categories"><a href="viewapp.php">View App Settings</a></li>
		</ul>
		<h3>Fiji Info</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="newFiji.php">Add New Info Fiji</a></li>
			<li class="icn_new_article"><a href="fijilang.php">Add Info Fiji Language</a></li>
			<li class="icn_categories"><a href="viewfiji.php">View Info Fiji</a></li>
		</ul>
		<h3>Fiji Video</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="newfjvd.php">Add New Video</a></li>
			<li class="icn_new_article"><a href="fjvdlang.php">Add Video Language</a></li>
			<li class="icn_categories"><a href="viewfjvd.php">View Video Info</a></li>
		</ul>
		<h3>Hotel</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="newhotel.php">Add New Hotel</a></li>
			<li class="icn_new_article"><a href="hotellang.php">Add Hotel Language</a></li>
			<li class="icn_categories"><a href="viewhotel.php">View Hotel</a></li>
		</ul>
		<h3>Information</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="newinfo.php">Add Information</a></li>
			<li class="icn_new_article"><a href="infolang.php">Add Information Language</a></li>
			<li class="icn_categories"><a href="viewinfo.php">View Information</a></li>
		</ul>
		<h3>Legal</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="newlegal.php">Add New Legal</a></li>
			<li class="icn_new_article"><a href="legallang.php">Add Legal Language</a></li>
			<li class="icn_categories"><a href="viewlegal.php">View Legal Info</a></li>
		</ul>
		<h3>Pre Arrival</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="newpre.php">Add Pre Arrival</a></li>
			<li class="icn_new_article"><a href="prelang.php">Add Pre Arrival Language</a></li>
			<li class="icn_categories"><a href="viewpre.php">View Pre Arrival</a></li>
		</ul>
		<h3>Stories/Tales</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="newsto.php">Add Stories/Tales</a></li>
			<li class="icn_new_article"><a href="stolang.php">Add Stories/Tales Language</a></li>
			<li class="icn_categories"><a href="viewsto.php">View Stories/Tales</a></li>
		</ul>
		<h3>Transdictionary</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="newtransdic.php">Add Transdictionary</a></li>
			<li class="icn_new_article"><a href="newtranslang.php">Add Transdictionary language</a></li>
			<li class="icn_categories"><a href="viewtransdic.php">View Transdictionary</a></li>
		</ul>
		<h3>Transfer</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="newtransfer.php">Add Transfer</a></li>
			<li class="icn_new_article"><a href="translang.php">Add Transfer Language</a></li>
			<li class="icn_categories"><a href="viewtransfer.php">View Transfer</a></li>
		</ul>

		<h3>Push Notification</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="addpushnotification.php">Add Push Notification</a></li>
			<li class="icn_categories"><a href="viewpushnotification.php">View Push Notification </a></li>
		</ul>


		<h3>Admin</h3>
		<ul class="toggle">
			<li class="icn_jump_back"><a href="logout.php">Logout</a></li>
		</ul>
		
		<footer>
			<hr />
			<p><strong>Copyright &copy; 2018 iTvTi</strong></p>
			<p>Website by <a href="http://itvti.com.fj/"> iTvTi</a></p>
		</footer>
	</aside><!-- end of sidebar -->
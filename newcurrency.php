<?php 
session_start();

include_once"config.php";
if(!isset($_SESSION['username']) || !isset($_SESSION['password'])){
	header("Location: login.php");
}else{
//$fetch_users_data = mysql_fetch_object(mysql_query("SELECT * FROM `members` WHERE username='".$_SESSION['username']."'"));
}
?>
<?php
header('Content-Type: text/html; charset=utf8_general_ci');

?>
<?php 

        mysqli_set_charset($conn,"utf8");
		$result = $conn->query("SELECT * FROM `currency`"); 
					
?>

<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8"/>
	<meta name="author" content="Bhawick Ghutla">

	<title>Currency</title>
	
	<link rel="stylesheet" href="css/layout.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="css/style.css">
	<!--[if lt IE 9]>
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="js/jquery-1.5.2.min.js" type="text/javascript"></script>
	<script src="js/hideshow.js" type="text/javascript"></script>
	<script src="js/jquery.tablesorter.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery.equalHeight.js"></script>
	<script type="text/javascript">
	$(document).ready(function() 
    	{ 
      	  $(".tablesorter").tablesorter(); 
   	 } 
	);
	$(document).ready(function() {

	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});
    </script>

    <script type="text/javascript">
    $(function(){
        $('.column').equalHeight();
    });
</script>
<!-- Function for Showing FileUpload option -->
<script type="text/javascript">
    function ShowHideDiv(chkImage) {
        var dvPassport = document.getElementById("dvPassport");
        dvPassport.style.display = chkImage.checked ? "block" : "none";
    }
</script>
</head>


<body>

	<header id="header">
		<hgroup>
			<h1 class="site_title"><a href="index.php">Feejee Traveller</a></h1>
			<h2 class="section_title">Currency </h2>
		</hgroup>
	</header> <!-- end of header bar -->
	
	<section id="secondary_bar">
		<div class="user">
			<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
		</div>
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="index.php">Feejee Traveller</a> <div class="breadcrumb_divider"></div> <a class="current">Currency</a></article>
		</div>
	</section><!-- end of secondary bar -->
	
	<?php include('navBar.php'); ?>
	

		
	
		
		
	
	<section id="main" class="column">
		</article><!-- end of content manager article -->
		<div class="clear"></div>
		
		<article class="module width_full">
			<header><h3>Currency</h3></header>
				<form action="addcurrency.php" method="post" enctype="multipart/form-data">
					<div class="module_content">
					<!-- Data Setting starts here -->
							<fieldset style="width:100%; float:left; margin-right: 3%;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>USD Rate </label>
								<input type="text" name="rate" value="" style="width:92%;" > 
								
							</fieldset>

							
							
							
				
							
							
							<div class="clear"></div>

					</div>
					<footer>
						<div class="submit_link">
							<input type="submit" value="Update" class="alt_btn">
						</div>
					</footer>
				</form>
			
		</article><!-- end of post new article -->

		<article class="module width_full">
		<header><h3 class="tabs_involved">History</h3>
		
		</header>

		<div class="tab_container">
			<div id="tab1" class="tab_content">
			<table class="tablesorter" cellspacing="0"> 
			<thead> 
				<tr> 
   		
    				<th>currency </th> 
    				<th>Rate</th> 
    				<th>By</th> 
    				<th>Updated On</th> 
				</tr> 
			</thead> 
			<tbody> 
			<?php
					
			while($row = $result->fetch_assoc()){ 
				echo "<tr> " ;   		
    
				echo "<td valign='top'>" . nl2br( $row['currency_code']) . "</td>";  
				echo "<td valign='top'>" . nl2br( $row['currency_rate']) . "</td>";  
				echo "<td valign='top'>" . nl2br( $row['lastmodifiedby']) . "</td>";  
				echo "<td valign='top'>" . nl2br( $row['lastmodified']) . "</td>";  

				echo "</tr>"; 

			}				    

?>
				
			</tbody> 
			</table>
			
		</div><!-- end of .tab_container -->
		
		

		<div class="spacer"></div>
	</section>
</body>

</html>
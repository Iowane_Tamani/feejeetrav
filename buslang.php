<?php 

session_start();

include_once"config.php";
if(!isset($_SESSION['username']) || !isset($_SESSION['password'])){
	header("Location: login.php");
}else{
//$fetch_users_data = mysql_fetch_object(mysql_query("SELECT * FROM `members` WHERE username='".$_SESSION['username']."'"));
}
?>
<?php
header('Content-Type: text/html; charset=utf8_general_ci');


mysqli_set_charset($conn,"utf8");


?>

<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8"/>
	<meta name="author" content="Bhawick Ghutla">

	<title>Add Business Language</title>
	
	<link rel="stylesheet" href="css/layout.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="css/style.css">
	<!--[if lt IE 9]>
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="js/jquery-1.5.2.min.js" type="text/javascript"></script>
	<script src="js/hideshow.js" type="text/javascript"></script>
	<script src="js/jquery.tablesorter.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery.equalHeight.js"></script>
	<script type="text/javascript">
	$(document).ready(function() 
    	{ 
      	  $(".tablesorter").tablesorter(); 
   	 } 
	);
	$(document).ready(function() {

	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});
    </script>

    <script type="text/javascript">
    $(function(){
        $('.column').equalHeight();
    });
</script>
<!-- Function for Showing FileUpload option -->
<script type="text/javascript">
    function ShowHideDiv(chkImage) {
        var dvPassport = document.getElementById("dvPassport");
        dvPassport.style.display = chkImage.checked ? "block" : "none";
    }
</script>
</head>


<body>

	<header id="header">
		<hgroup>
			<h1 class="site_title"><a href="index.php">Feejee Traveller</a></h1>
			<h2 class="section_title">Business Language</h2>
		</hgroup>
	</header> <!-- end of header bar -->
	
	<section id="secondary_bar">
		<div class="user">
			<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
		</div>
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="index.php">Feejee Traveller</a> <div class="breadcrumb_divider"></div> <a class="current">Business Language</a></article>
		</div>
	</section><!-- end of secondary bar -->
	
	<?php include('navBar.php'); ?>
	
	<section id="main" class="column">

		<div class="clear"></div>
		
		<article class="module width_full">
			<header><h3>Add Business Language</h3></header>
				<form action="addbuslang.php" method="post" enctype="multipart/form-data">
					<div class="module_content">
					<!-- Data Setting starts here -->
						 <fieldset style="width:48%; float:left; "> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Select Business</label>
								<select style="width:92%;" name="business" id="business" >
								<?php 
								$resultq = $conn->query("SELECT * FROM `business` WHERE language_lang_id = '1' ORDER BY business_name ASC"); 

								while($row = $resultq->fetch_assoc()){ 						
	    						
	    								echo "<option value=\"" .$row['businessmulti_id']. "\">" . $row['business_name'] ."</option>" ;
								} 
									
								?>
								</select>
							</fieldset>
							
							<fieldset style="width:48%; float:right; "> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Business Name </label>
								<input type="text" name="business_name" value="" style="width:92%;" > 
							</fieldset>
							
							

							<fieldset style="width:48%; float:left; ">
								<label>Description</label>
								<textarea rows="5" name="business_description" id="content" style="width:92%;"></textarea>
								<!--<label> Description Chinese</label>
								<textarea rows="5" name="business_description_zh" id="content" style="width:92%;"> </textarea>-->
							</fieldset>
							<fieldset style="width:48%; float:right; ">
								<label>Business Service</label>
								<textarea rows="5" name="business_service" id="content" style="width:92%;"></textarea>
								<!--<label> Description Chinese</label>
								<textarea rows="5" name="business_description_zh" id="content" style="width:92%;"> </textarea>-->
							</fieldset>
							

							
							<fieldset style="width:48%; float:left; "> <!-- to make two field float next to one another, adjust values accordingly -->
							<label>Lanuage Type</label>
								<select style="width:92%;" name="language" id="language" >
								<?php 
								$resultq = $conn->query("SELECT * FROM `language`"); 

								while($row = $resultq->fetch_assoc()){ 						
	    						
	    								echo "<option value=\"" .$row['lang_id']. "\">" . $row['lang_name'] ."</option>" ;
								} 
									
								?>
								</select>
							</fieldset>
							

							<!-- Data Setting Ends here -->
									<div class="clear"></div>

							
							
							<div class="clear"></div>
							
					</div>
					<footer>
						<div class="submit_link">
						
							<input type="submit" value="Add Business" class="alt_btn">
							<input type="submit" value="Reset">
						</div>
					</footer>
				</form>
			
		</article><!-- end of post new article -->

		<div class="spacer"></div>
	</section>
</body>

</html>
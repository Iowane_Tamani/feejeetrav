<?php  
session_start();

include_once"config.php";
if(!isset($_SESSION['username']) || !isset($_SESSION['password'])){
	header("Location: login.php");
}else{
//$fetch_users_data = mysql_fetch_object(mysql_query("SELECT * FROM `members` WHERE username='".$_SESSION['username']."'"));
}
 $id = $_GET['id'];
 $query2 = "SELECT `tour_image` FROM tba_tours WHERE `id`= $id ";
 $result = $conn->query($query2)or die(mysqli_error());

 while($row = $result->fetch_assoc()){ 
 	$business_image_url = $row['tour_image'];
 }

//  $business_image_url = 'http://feejeetraveller.com/tba/images/business/425/logo.jpg';
// getting the directory out of the url
$dir = str_replace("http://www.feejeetraveller.com/","",$business_image_url);
$dir = str_replace("http://feejeetraveller.com/","",$dir);
$myArray = explode('/', $dir);
$path = "";

$count = count($myArray);
for ($x = 0; $x < $count-1; $x++) {
    $path = $path . "/" .$myArray[$x];
} 

$dir = "..".$path;
//echo "$dir";
$files = preg_grep('~\.(jpeg|jpg|png|JPEG|JPG|PNG)$~', scandir($dir));


?>



<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8"/>
	<meta name="author" content="Bhawick Ghutla">

	<title>Tour gallery</title>
	
	<link rel="stylesheet" href="css/layout.css" type="text/css" media="screen" />

	<link rel="stylesheet" href="css/style.css">

    
    <link rel='stylesheet prefetch' href='http://drop-uploader.borisolhor.com/pe-icon-7-stroke/css/pe-icon-7-stroke.css'>
<link rel='stylesheet prefetch' href='http://drop-uploader.borisolhor.com/css/normalize.css'>


	<!--[if lt IE 9]>
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="js/jquery-1.5.2.min.js" type="text/javascript"></script>
	<script src="js/hideshow.js" type="text/javascript"></script>
	<script src="js/jquery.tablesorter.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery.equalHeight.js"></script>

</head>


<body>

	<header id="header">
		<hgroup>
			<h1 class="site_title"><a href="index.php">Feejee Traveller</a></h1>
			<h2 class="section_title">Tour gallery </h2></div>
		</hgroup>
	</header> <!-- end of header bar -->
	
	<section id="secondary_bar">
		<div class="user">
			<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
		</div>
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="index.php">Feejee Traveller</a> <div class="breadcrumb_divider"></div> <a class="current">Tour Gallery</a></article>
		</div>
	</section><!-- end of secondary bar -->
	
	<?php include('navBar.php'); ?>
	
	<section id="main" class="column">

		<div class="clear"></div>
		
		
	<article class="module width_full">
			<header><h3> Update Tour Image gallery</h3></header>
    <fieldset style="width:98%; float:left; margin-right: 3%;"> 
    <div class="container">
        <div class="row">
            <div class="twelve column" style="margin-top: 5%">
               
                <h4 class="alert_info">Select BROWSE to upload images. You can select multiple images by holding down Ctrl Key</h4>
                <form action="updatetourgallery.php?id=<?php echo $id;?>" method="post" enctype="multipart/form-data">
                    <input type="file" name="files[]" multiple>
                    
                    <input class="button-primary" type="submit" value="Submit"> 
                    <!-- <input type="submit" value="Upload" class="alt_btn"> -->
                </form>

            </div>
        </div>
    </div>
    </fieldset>
    		<div class="clear"></div>


    		<script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>
			<script src='http://drop-uploader.borisolhor.com/js/drop_uploader.js'></script>

        	<script src="js/index.js"></script>

		<div class="spacer"></div>
</article>

<article class="module width_full">
		<header><h3 class="tabs_involved">Content Manager</h3>
	
		</header>

		<div class="tab_container">
			<div id="tab1" class="tab_content">
			<table class="tablesorter" cellspacing="0"> 
			<thead> 
				<tr> 
   			
   					<th></th> 
   					<th></th> 
   					<th></th> 

    				
				</tr> 
			</thead> 
			<tbody> 
			<?php 
			$i=0;	
			foreach ($files as &$value) {
				echo "<tr> " ;
//=====> UNCOMMENT
				$image= str_replace("..","http://www.feejeetraveller.com",$dir);
				//$image = $dir;
   					//echo "<td><input type='checkbox'></td>";
				if (!strcmp($value,$myArray[$count-1])) {
					echo "<td><img src =\"" ."$image/$value" . "\" width=\"300px\"/></td>";
    				echo "<td><img src =\"images/hero.png\"width=\"25\"/></td>";

    				//echo "<td>The hero image is: ".$myArray[$count-1]."</td>";
				}else{
					//echo "<td>$image/$value</td>";
    				echo "<td><img src =\"" ."$image/$value" . "\" width=\"300px\"/></td>";
    				echo"<td>". "<a href=\" deleteimage.php?item=" ."$dir/$value&id=$id". " \">" ."<input type='image' src='images/icn_trash.png' title='Trash'></td> ";

				}
    				
			}
			
						
			?>
				
			</tbody> 
			</table>
			</div><!-- end of #tab1 -->
			
			
		</div><!-- end of .tab_container -->
		
		</article><!-- end of content manager article -->
		
		<div class="spacer"></div>
	</section>
</body>

</html>
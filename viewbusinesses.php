 <?php
session_start();

include_once"config.php";
if(!isset($_SESSION['username']) || !isset($_SESSION['password'])){
	header("Location: login.php");
}else{
$fetch_users_data = $conn->query("SELECT * FROM `members` WHERE username='".$_SESSION['username']."'");
}
?>

<?php

       
         mysqli_set_charset($conn,"utf8");
		$result = $conn->query("SELECT * FROM business WHERE `business_available`=1 and language_lang_id=1 ORDER BY business_name ASC");
								
								
								
					
?>
<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8"/>
	<meta name="author" content="Bhawick Ghutla">

	<title>Businesses</title>

	<link rel="stylesheet" href="css/layout.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="css/style.css">
	<!--[if lt IE 9]>
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="js/jquery-1.5.2.min.js" type="text/javascript"></script>
	<script src="js/hideshow.js" type="text/javascript"></script>
	<script src="js/jquery.tablesorter.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery.equalHeight.js"></script>
	<script type="text/javascript">
	$(document).ready(function() 
    	{ 
      	  $(".tablesorter").tablesorter(); 
   	 } 
	);
	$(document).ready(function() {

	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});
    </script>

    <script type="text/javascript">
    $(function(){
        $('.column').equalHeight();
    });
</script>
<!-- Function for Showing FileUpload option -->
<script type="text/javascript">
    function ShowHideDiv(chkImage) {
        var dvPassport = document.getElementById("dvPassport");
        dvPassport.style.display = chkImage.checked ? "block" : "none";
    }
</script>
</head>


<body>

	<header id="header">
		<hgroup>
			<h1 class="site_title"><a href="index.php">Feejee Traveller</a></h1>
			<h2 class="section_title">Businesses </h2>
		</hgroup>
	</header> <!-- end of header bar -->
	
	<section id="secondary_bar">
		<div class="user">
			<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
		</div>
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="index.php">Feejee Traveller</a> <div class="breadcrumb_divider"></div> <a class="current">Businesses</a></article>
		</div>
	</section><!-- end of secondary bar -->
	<!-- including navigation Bar -->
	<?php include('navBar.php'); ?>
	
	<section id="main" class="column">

		<div class="clear"></div>
		
		<article class="module width_full">
		<header><h3 class="tabs_involved">Content Manager</h3>
		<ul class="tabs">
   			<li><a href="#tab1">Active</a></li>
    		<li><a href="#tab2">Inactive</a></li>
		</ul>
		</header>

		<div class="tab_container">
			<div id="tab1" class="tab_content">
			<table class="tablesorter" cellspacing="0"> 
			<thead> 
				<tr> 
   			
   					<th></th> 
    				<th>Entry Name</th> 
    				<!--<th>Branches</th>--> 
    				<th>Actions</th> 
    		    	<th>Gallery</th> 

				</tr> 
			</thead> 
			<tbody> 
			<?php
					
			while($row = $result->fetch_assoc()) {
				echo "<tr> " ;

   					//echo "<td><input type='checkbox'></td>";
   				
    				echo "<td><img src =\"" . $row['business_image']. "\" width=\"100px\"/></td>";
    				echo "<td><font size=\"4\">" ."<a class = \"titleLink\" href=\" detailbusiness.php?id=" .$row['businessmulti_id']. " \">". $row['business_name'] . "</font>";

    		
    				//echo "<td><br><a href=\" setbusdetails.php?id=" .$row['business_id']. " \"> Add Branch</a>";
    				
    				echo"<td>". "<a href=\" detailbusiness.php?id=" .$row['businessmulti_id']. " \">" ."<input type=\"image\" src=\"images/icn_edit.png\" title=\"Edit\" >" . "<a href=\" deletebusiness.php?id=" .$row['businessmulti_id']. " \">" ."<input type='image' src='images/icn_trash.png' title='Deactivate'></td> ";
					echo "<td><a href=\" businessgallery.php?id=" .$row['businessmulti_id']. " \">" ."<input type='image' src='images/gallery.png' title='gallery' width =\"25px\"></td>";
				echo "</tr>"; 

			}
						
			?>
				
			</tbody> 
			</table>
			</div><!-- end of #tab1 -->
			
			<div id="tab2" class="tab_content">
			<?php 
				 mysqli_set_charset($conn,"utf8");
		$inactive = $conn->query("SELECT * FROM business WHERE `business_available`=0 ORDER BY business_name ASC");
			 ?>
			<table class="tablesorter" cellspacing="0"> 
				<thead> 
				<tr> 
   			
   					<th></th> 
    				<th>Entry Name</th> 
<!--     				<th>Branches</th> 
 -->    				<th>Actions</th> 
				</tr> 
			</thead> 
			<tbody> 
			<?php
					
			while($row = $inactive->fetch_assoc()){ 
				echo "<tr> " ;

   					//echo "<td><input type='checkbox'></td>";
   				
    				echo "<td><img src =\"" . $row['business_image']. "\" width=\"100px\"/></td>";
    				echo "<td><font size=\"4\">" . $row['business_name'] . "</font>";
    				
    				//echo "<td>" . $row['last_modified'] . "</td>";
    				//echo "<br><a href=\" setbusdetails.php?id=" .$row['business_id']. " \"> Add Branch</a>". " <br><a href=\" detailbranches.php?id=" .$row['business_id']. " \"> View Branch" ."</td> ";

    				echo"<td>". "<a href=\" activatebusiness.php?id=" .$row['business_id']. " \">" ."<input type='image' src='images/icn_alert_success.png' title='Trash'> Restore</td> ";

				echo "</tr>"; 

			}
						
			?>
				
			</tbody> 
			</table>

			</div><!-- end of #tab2 -->
			
		</div><!-- end of .tab_container -->
		
		</article><!-- end of content manager article -->
		
		<div class="spacer"></div>
	</section>
</body>

</html>
<?php 
session_start();

include_once"config.php";
if(!isset($_SESSION['username']) || !isset($_SESSION['password'])){
	header("Location: login.php");
}else{
//$fetch_users_data = mysql_fetch_object(mysql_query("SELECT * FROM `members` WHERE username='".$_SESSION['username']."'"));
}
?>
<?php
header('Content-Type: text/html; charset=utf8_general_ci');
//$id = $_GET['id'];

//mysqli_set_charset($conn,"utf8");
//$result = $conn->query("SELECT * FROM tba_tours WHERE id = '$id'"); 

?>

<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8"/>
	<meta name="author" content="Bhawick Ghutla">

	<title>New Advertisement</title>
	
	<link rel="stylesheet" href="css/layout.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="css/style.css">
	<!--[if lt IE 9]>
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="js/jquery-1.5.2.min.js" type="text/javascript"></script>
	<script src="js/hideshow.js" type="text/javascript"></script>
	<script src="js/jquery.tablesorter.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery.equalHeight.js"></script>
	<script type="text/javascript">
	$(document).ready(function() 
    	{ 
      	  $(".tablesorter").tablesorter(); 
   	 } 
	);
	$(document).ready(function() {

	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});
    </script>

    <script type="text/javascript">
    $(function(){
        $('.column').equalHeight();
    });
</script>
<!-- Function for Showing FileUpload option -->
<script type="text/javascript">
    function ShowHideDiv(chkImage) {
        var dvPassport = document.getElementById("dvPassport");
        dvPassport.style.display = chkImage.checked ? "block" : "none";
    }
</script>
</head>


<body>

	<header id="header">
		<hgroup>
			<h1 class="site_title"><a href="index.php">Feejee Traveller</a></h1>
			<h2 class="section_title">New Advertisement </h2></div>
		</hgroup>
	</header> <!-- end of header bar -->
	
	<section id="secondary_bar">
		<div class="user">
			<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
		</div>
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="index.php">Feejee Traveller</a> <div class="breadcrumb_divider"></div> <a class="current">New Advertisement</a></article>
		</div>
	</section><!-- end of secondary bar -->
	
	<?php include('navBar.php'); ?>
	
	<section id="main" class="column">

		<div class="clear"></div>
		
		<article class="module width_full">
			<header><h3>Add New Event</h3></header>
				<form action="addAdvert.php" method="post" enctype="multipart/form-data">
					<div class="module_content">
					<!-- Data Setting starts here -->
					
							<fieldset style="width:48%; float:left; margin-right: 3%;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Business</label>
								<select style="width:92%;" name="business_id" id="category" >
									<?php 
									$result = $conn->query("SELECT `business_id`, `business_name` FROM `tba_businesses` ORDER BY `business_name` ASC"); 

									while($row = $result->fetch_assoc()){ 

		    						echo $row['id'] . $row['category'];
		    								echo "<option value=\"" .$row['business_id']. "\">" . $row['business_name'] ."</option>" ;
									} 
										
									?>
								</select>
							</fieldset>
							<fieldset style="width:48%; float:left;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Subsription Type</label>
								<select style="width:92%;" name="banner_type_id" id="category" >
									<?php 
									$result = $conn->query("SELECT * FROM `tba_banner_type`"); 

									while($row = $result->fetch_assoc()){ 							

		    								echo "<option value=\"" .$row['banner_type_id']. "\">" . $row['banner_type_name'] ."</option>" ;
									} 
										
									?>
								</select>
							</fieldset>
							<fieldset style="width:48%; float:left;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label> Placement</label>
								<select style="width:92%;" name="activity_page_id" id="category" >
									<?php 
									$result = $conn->query("SELECT * FROM `tba_activity_page`"); 

									while($row = $result->fetch_assoc()){ 							

		    								echo "<option value=\"" .$row['activity_page_id']. "\">" . $row['activity_page_name'] ."</option>" ;
									} 
										
									?>
								</select>
							</fieldset>
							<fieldset style="width:48%; float:right;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Banner link</label>
								<input type="text" name="banner_link" value="" style="width:92%;">
							</fieldset><div class="clear"></div>	
						
								<fieldset style="width:30%; float:left; margin-left: 10%;"> <!-- to make two field float next to one another, adjust values accordingly -->
							
										<label> Start Date </label>
											<input type = "date" name="banner_start_date" style="width:92%; float: left; margin-left: 3%;" >
								</fieldset>
								<fieldset style="width:30%; float:right; margin-right: 10%;"> <!-- to make two field float next to one another, adjust values accordingly -->
										<label>End Date </label>
										
											<input type = "date" name="banner_end_date" style="width:92%; float: left; margin-left: 3%;" >
								</fieldset>

								<fieldset style="width:48%; float:left;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Phone Advertisement Image  </label>
									<fieldset style="width:98%; float:left; margin-left: 1%"> <!-- to make two field float next to one another, adjust values accordingly -->
										<label> English  </label>
										
										<input type = "file" name="banner_phone_en" style="width:92%; float: left; margin-left: 3%;" >
									</fieldset>
									<fieldset style="width:98%; float:left; margin-left: 1%"> <!-- to make two field float next to one another, adjust values accordingly -->
										<label>Chinese </label>
										
										<input type = "file" name="banner_phone_zh" style="width:92%; float: left; margin-left: 3%;" >
									</fieldset>

								</fieldset>
								<fieldset style="width:48%; float:right; "> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Tablet Advertisement Image  </label>
									<fieldset style="width:98%; float:left; margin-left: 1%"> <!-- to make two field float next to one another, adjust values accordingly -->
										<label> English  </label>
										
										<input type = "file" name="banner_tablet_en" style="width:92%; float: left; margin-left: 3%;" >
									</fieldset>
									<fieldset style="width:98%; float:left; margin-left: 1%"> <!-- to make two field float next to one another, adjust values accordingly -->
										<label>Chinese </label>
										
										<input type = "file" name="banner_tablet_zh" style="width:92%; float: left; margin-left: 3%;" >
									</fieldset>

								</fieldset>
								
							
							
							<!-- Data Setting Ends here -->

							
							<div class="clear"></div>
							
					</div>
					<footer>
						<div class="submit_link">
						
							<input type="submit" value="Add Advertisement" class="alt_btn">
							<input type="submit" value="Reset">
						</div>
					</footer>
				</form>
			
		</article><!-- end of post new article -->

		<div class="spacer"></div>
	</section>
</body>

</html>
<?php  
session_start();

include_once"config.php";
if(!isset($_SESSION['username']) || !isset($_SESSION['password'])){
	header("Location: login.php");
}else{
//$fetch_users_data = mysql_fetch_object(mysql_query("SELECT * FROM `members` WHERE username='".$_SESSION['username']."'"));
}
?>
<?php 
header('Content-Type: text/html; charset=utf8_general_ci');
$id = $_GET['id'];

?>
<?php  

        mysqli_set_charset($conn,"utf8");
		$result = $conn->query("SELECT `trates_id`,`tours`.tour_name,`trates_prenote`,`trates_type`,`trates_cost`,`trates_display`,`tours_multitour_id` FROM `tour_rates`,`tours` WHERE `tour_rates`.`tours_multitour_id` = `tours`.tourmulti_id AND `tours_multitour_id` = $id  AND `tour_rates`.`trates_display` = 1"); 
					
?>

<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8"/>
	<meta name="author" content="Bhawick Ghutla">

	<title>Tour  Rates</title>
	
	<link rel="stylesheet" href="css/layout.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="css/style.css">
	<!--[if lt IE 9]>
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="js/jquery-1.5.2.min.js" type="text/javascript"></script>
	<script src="js/hideshow.js" type="text/javascript"></script>
	<script src="js/jquery.tablesorter.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery.equalHeight.js"></script>
	<script type="text/javascript">
	$(document).ready(function() 
    	{ 
      	  $(".tablesorter").tablesorter(); 
   	 } 
	);
	$(document).ready(function() {

	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});
    </script>

    <script type="text/javascript">
    $(function(){
        $('.column').equalHeight();
    });
</script>
<!-- Function for Showing FileUpload option -->
<script type="text/javascript">
    function ShowHideDiv(chkImage) {
        var dvPassport = document.getElementById("dvPassport");
        dvPassport.style.display = chkImage.checked ? "block" : "none";
    }
</script>
</head>


<body>

	<header id="header">
		<hgroup>
			<h1 class="site_title"><a href="index.php">Feejee Traveller</a></h1>
			<h2 class="section_title">Edit Branch </h2>
		</hgroup>
	</header> <!-- end of header bar -->
	
	<section id="secondary_bar">
		<div class="user">
			<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
		</div>
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="index.php">Feejee Traveller</a> <div class="breadcrumb_divider"></div> <a class="current">Branch</a></article>
		</div>
	</section><!-- end of secondary bar -->
	
	<?php include('navBar.php'); ?>
	

		
		
		
		
	
	<section id="main" class="column">

		<div class="clear"></div>

		<article class="module width_full">
			<header><h3>Branch</h3></header>
				<form action="addbranlang.php?id= <?php  echo $id;?> " method="post" enctype="multipart/form-data">
					<div class="module_content">
					<!-- Data Setting starts here -->
					
							<fieldset style="width:48%; float:left;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Branch Name </label>
								<input type="text" name="branch_name" value="" style="width:92%;" > 
								
							</fieldset>
							<fieldset style="width:48%; float:right;">
								<label>Branch Description</label>
								<textarea rows="5" name="branch_description" id="content" style="width:92%;"></textarea>
								
							</fieldset>
							<fieldset style="width:48%; float:left; margin-right: 3%;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Branch Location </label>
								<input type="text" name="branch_location" value="" style="width:92%;" > 
								
							</fieldset>
							<fieldset style="width:48%; float:right;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Branch Address </label>
								<input type="text" name="branch_address" value="" style="width:92%;" > 
								
							</fieldset>
							<fieldset style="width:48%; float:left;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Latitude </label>
								<input type="text" name="latitude" value="" style="width:92%;" > 
								
							</fieldset>
							<fieldset style="width:48%; float:right;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Longitude</label>
								<input type="text" name="longitude" value="" style="width:92%;" > 
								
							</fieldset>
							
							<fieldset style="width:48%; float:left;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Branch Phone</label>
								<input type="text" name="branch_phone" value="" style="width:92%;" > 
								
							</fieldset>
							<fieldset style="width:48%; float:right;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Branch Mobile</label>
								<input type="text" name="branch_mobile" value="" style="width:92%;" > 
								
							</fieldset>
							<fieldset style="width:48%; float:left;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Branch Email</label>
								<input type="text" name="branch_email" value="" style="width:92%;" > 
								
							</fieldset>
							<fieldset style="width:48%; float:right;">
								<label>Branch Services</label>
								<textarea rows="5" name="branch_services" id="content" style="width:92%;"></textarea>
								
							</fieldset>
							<fieldset style="width:48%; float:left;">
								<label>Opening Hours</label>
								<textarea rows="5" name="branch_hours" id="content" style="width:92%;"></textarea>
								
							</fieldset>
							<fieldset style="width:48%; float:right;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Video URL</label>
								<input type="text" name="branch_video" value="" style="width:92%;" > 
								
							</fieldset>
							<fieldset style="width:48%; float:left;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Branch Form URL</label>
								<input type="text" name="branch_formurl" value="" style="width:92%;" > 
								
							</fieldset>
							<fieldset style="width:48%; float:right;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Category</label>
								<select style="width:92%;" name="branch_category" id="branch_category" >
								<?php 
								$resultq = $conn->query("SELECT * FROM `business_category`"); 

								while($row = $resultq->fetch_assoc()){ 						
	    								echo "<option value=\"" .$row['catmulti_id']. "\">" . $row['cat_name'] ."</option>" ;
								} 
									
								?>
								</select>
							</fieldset>

							<fieldset style="width:48%; float:left; "> <!-- to make two field float next to one another, adjust values accordingly -->
							<label>Lanuage Type</label>
								<select style="width:92%;" name="language" id="language" >
								<?php 
								$resultq = $conn->query("SELECT * FROM `language`"); 

								while($row = $resultq->fetch_assoc()){ 						
	    						
	    								echo "<option value=\"" .$row['lang_id']. "\">" . $row['lang_name'] ."</option>" ;
								} 
									
								?>
								</select>
							</fieldset>
							
							
							
				
						
							
							<div class="clear"></div>

							<fieldset><label for="chkImage">
								<input type="checkbox" name="chkImage" id="chkImage" value="yes" onclick="ShowHideDiv(this)" />
								    Select to upload Image
								</label>
							</fieldset>
							
							<div id="dvPassport" style="display: none">
								<fieldset>
									
									    <script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
										    <div class="file-upload">
										      <button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Add Image</button>

										      <div class="image-upload-wrap">
										        <input class="file-upload-input" type='file' name="UploadImage" id = "UploadImage" onchange="readURL(this);" accept="image/*" />
										        <div class="drag-text">
										          <h3>Drag and drop a file or select add Image</h3>
										        </div>
										      </div>
										      <div class="file-upload-content">
										        <img class="file-upload-image" src="#" alt="your image" />
										        <div class="image-title-wrap">
										          <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
										        </div>
										      </div>
										    </div>

 										 <script src="js/index.js"></script>
									
								</fieldset>
							</div>
							<div class="clear"></div>

					</div>
					<footer>
						<div class="submit_link">
							<input type="submit" value="Add" class="alt_btn">
							<input type="submit" name="done" value="DONE">
						</div>
					</footer>
				</form>
			
		</article><!-- end of post new article -->

		<div class="spacer"></div>
	</section>
</body>

</html>
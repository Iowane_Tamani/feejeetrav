<?php 
session_start();
include_once"config.php";
if(!isset($_SESSION['username']) || !isset($_SESSION['password']))
{
	header("Location: login.php");
}
else{
//$fetch_users_data = mysql_fetch_object(mysql_query("SELECT * FROM `members` WHERE username='".$_SESSION['username']."'"));
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">

<head>
	<meta charset="utf-8"/>
	<meta name="author" content="Bhawick Ghutla">
	<title>Feejee Traveller</title>
	
	<link rel="stylesheet" href="css/layout.css" type="text/css" media="screen" />
	<!--[if lt IE 9]>
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<script src="js/jquery-1.5.2.min.js" type="text/javascript"></script>
	<script src="js/hideshow.js" type="text/javascript"></script>
	<script src="js/jquery.tablesorter.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery.equalHeight.js"></script>
	<script type="text/javascript">
	$(document).ready(function() 
    	{ 
      	  $(".tablesorter").tablesorter(); 
   	 } 
	);
	$(document).ready(function() {

	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});
    </script>
    <script type="text/javascript">
    $(function(){
        $('.column').equalHeight();
    });
</script>

</head>


<body>

	<header id="header">
	
		<hgroup>
			<h1 class="site_title"><a href="index.php">Feejee Traveller</a></h1>
			<h2 class="section_title">Dashboard</h2>
		</hgroup>
	</header> <!-- end of header bar -->
	
	<section id="secondary_bar">
		<div class="user">
			<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
		</div>
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="index.php">Feejee Traveller</a> <div class="breadcrumb_divider"></div> <a class="current">Dashboard</a></article>
		</div>
	</section><!-- end of secondary bar -->
	
	<?php include('navBar.php'); ?>
	<section id="main" class="column">
		
	
		
		<article class="module width_full">
			<header><h3>Welcome to the Feejee traveller Portal</h3></header>
			<div class="module_content">


			
		
				<?php
                  $countBus = countBus($conn);
                  
                  if ($countBus->num_rows > 0){
                  
                    while($row = $countBus->fetch_assoc()){
                  
                       
                  ?>
				<article class="stats_overview" id="1">
					
				<div class="overview_previous" align="center">
						<p class="overview_day">Businesses</p>
						<p class="overview_count"><?php echo $row['countBusiness']; ?></p>
						<p class="overview_type"><a href="viewbusinesses.php" >View</a></p>
					</div>
					
				</article>
				<!-- Total number of business views  -->
				 <?php } } ?>


				 


				<?php
                 $countTour = countTour($conn);
                  
                  if ($countTour->num_rows > 0){
                  
                    while($row = $countTour->fetch_assoc()){
                  
                       
                  ?>
					<article class="stats_overview">
					
					<div class="overview_previous">
						<p class="overview_day">Tours</p>
						<p class="overview_count"><?php echo $row['countTour']; ?></p>
						<p class="overview_type"><a href="viewTours.php" >View</a></p>
					</div>
					
				</article>  <?php } } ?>
				
			

		
	<div class="clear"></div>
				<!--article class="stats_overview">
				<div class="overview_today">
						<p class="overview_day">User</p>
						<p class="overview_count">1,634</p>
						<p class="overview_type">Active</p>
						<p class="overview_count">5,134</p>
						<p class="overview_type">Nonactive</p>
					</div>
				</article-->
				<div class="clear"></div>
			</div>
		</article><!-- end of stats article -->
		
		<!-- Top 10 Trending businesses in Fiji -->

		<article class="module width_full">
			<header><h3>Trending Businesses</h3></header>
			<div class="module_content">

				 <?php
                  $getbuscount = getbuscount($conn);
                  
                  if ($getbuscount->num_rows > 0){
                  
                    while($row = $getbuscount->fetch_assoc()){
                  
                       
				// var_dump($getbuscount);
 				// 	exit;
                  ?>

				<article class='stats_overview'>
					
					<div class='overview_previous'>
						<p class='overview_day'><?php echo $row['business_name']; ?></p>
						<p class='overview_count'><?php echo $row['business_count']; ?></p>
						<!-- <p class='overview_type'><a href='newcurrency.php' >Edit</a></p> -->
					</div>

			
				</article> 
					
				<?php } } ?>
					
			

			
	<div class="clear"></div>
				<!--article class="stats_overview">
				<div class="overview_today">
						<p class="overview_day">User</p>
						<p class="overview_count">1,634</p>
						<p class="overview_type">Active</p>
						<p class="overview_count">5,134</p>
						<p class="overview_type">Nonactive</p>
					</div>
				</article-->
				<div class="clear"></div>
			</div>
		</article><!-- end of stats article -->


		<article class="module width_full">
			<header><h3>Currency Rates</h3></header>
			<div class="module_content">


				
				<?php

	 			mysqli_set_charset($conn,"utf8");
				$result = $conn->query("SELECT * FROM `currency` ORDER BY lastmodified DESC LIMIT 1"); 
				while($row = $result->fetch_assoc()){ 
					$rateUsd = $row['currency_rate'] ;
					
			}
		
				echo	"<article class='stats_overview'>
					
					<div class='overview_previous'>
						<p class='overview_day'>USD Rate</p>
						<p class='overview_count'> $".$rateUsd."</p>
						<p class='overview_type'><a href='newcurrency.php' >Edit</a></p>
					</div>
					
				</article>  " ;
					
				
			?>

			
	<div class="clear"></div>
				<!--article class="stats_overview">
				<div class="overview_today">
						<p class="overview_day">User</p>
						<p class="overview_count">1,634</p>
						<p class="overview_type">Active</p>
						<p class="overview_count">5,134</p>
						<p class="overview_type">Nonactive</p>
					</div>
				</article-->
				<div class="clear"></div>
			</div>
		</article><!-- end of stats article -->
		
		
	</section>


</body>

</html>
<?php



function countBus($conn){
     return $conn->query("SELECT COUNT(business_id) as 'countBusiness' FROM business WHERE business_available = 1");
     					  
   }



function countTour($conn){
     return $conn->query("SELECT COUNT(tourmulti_id) as 'countTour' FROM tours WHERE tour_listingtype = 1");

 }


 function getbuscount($conn){
     return $conn->query("SELECT a.businessmulti_id, a.business_name, count(b.business_business_id) AS 'business_count' FROM business_vlogs b, business a WHERE b.business_business_id = a.businessmulti_id AND a.business_listingtype_list_id = '1' GROUP BY b.business_business_id ORDER BY business_count DESC LIMIT 1");
   
   }


?>
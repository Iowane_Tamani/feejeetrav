<?php 
session_start();

include_once"config.php";
if(!isset($_SESSION['username']) || !isset($_SESSION['password'])){
	header("Location: login.php");
}else{
//$fetch_users_data = mysql_fetch_object(mysql_query("SELECT * FROM `members` WHERE username='".$_SESSION['username']."'"));
}
?>
<?
header('Content-Type: text/html; charset=utf8_general_ci');
$id = $_GET['id'];
$rid = $_GET['rid'];

?>
<?php 

        mysqli_set_charset($conn,"utf8");
		$result = $conn->query("SELECT `tba_businesses`.business_name,`tba_business_details`.id, `tba_business_details`.business_id,`business_category`,`business_location`,`business_address`,`business_latitude`, `business_longitude`, `business_contact_num` FROM `tba_businesses`,`tba_business_details` WHERE `tba_business_details`.business_id = `tba_businesses`.business_id AND `tba_business_details`.`business_id` =$id AND `tba_business_details`.`business_available` =1 ");
		
		$sql=mysqli_query($conn,"SELECT * FROM tba_business_details WHERE id = $rid");
                //{
                        //echo("Error description: " . mysqli_error($conn));
                //}
		//$sql = $conn->query("SELECT * FROM `tba_business_details` WHERE `id` = $rid"); 
?>
					


<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8"/>

	<title>Branch</title>
	
	<link rel="stylesheet" href="css/layout.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="css/style.css">
	<!--[if lt IE 9]>
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="js/jquery-1.5.2.min.js" type="text/javascript"></script>
	<script src="js/hideshow.js" type="text/javascript"></script>
	<script src="js/jquery.tablesorter.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery.equalHeight.js"></script>
	<script type="text/javascript">
	$(document).ready(function() 
    	{ 
      	  $(".tablesorter").tablesorter(); 
   	 } 
	);
	$(document).ready(function() {

	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});
    </script>

    <script type="text/javascript">
    $(function(){
        $('.column').equalHeight();
    });
</script>
<!-- Function for Showing FileUpload option -->
<script type="text/javascript">
    function ShowHideDiv(chkImage) {
        var dvPassport = document.getElementById("dvPassport");
        dvPassport.style.display = chkImage.checked ? "block" : "none";
    }
</script>
</head>


<body>

	<header id="header">
		<hgroup>
			<h1 class="site_title"><a href="index.php">Feejee Traveller</a></h1>
			<h2 class="section_title">Business Branches </h2>
		</hgroup>
	</header> <!-- end of header bar -->
	
	<section id="secondary_bar">
		<div class="user">
			<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
		</div>
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="index.php">Feejee Traveller</a> <div class="breadcrumb_divider"></div> <a class="current">Branch</a></article>
		</div>
	</section><!-- end of secondary bar -->
	
	<?php include('navBar.php'); ?>
	

		
	
		
		
	
	<section id="main" class="column">
		</article><!-- end of content manager article -->
		<div class="clear"></div>
		<?php 		
			if($sql){	
			while($line = $sql->fetch_assoc()){ 
			
			$categoryID = $line['business_category'];
												
		?>
		<article class="module width_full">
			<header><h3>Business</h3></header>
				<form action="editbranch.php?id= <?php echo $rid . "&bid=" . $id;?> " method="post" enctype="multipart/form-data">
					<div class="module_content">
					
					<!-- Data Setting starts here -->
							<fieldset style="width:100%; float:left; margin-right: 3%;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Address </label>
								<input type="text" name="business_address" value="<?php echo $line['business_address']; ?>" style="width:92%;" > 
								
							</fieldset>

							<fieldset style="width:48%; float:left; margin-right: 3%;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>City </label>
								<input type="text" name="business_location" value="<?php echo $line['business_location']; ?>" style="width:92%;" > 
								
							</fieldset>

							<fieldset style="width:48%; float:left; "> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Category</label>
								<select style="width:92%;" name="business_category" id="category" >
									<?php 
									$resultq = $conn->query("SELECT * FROM tba_category"); 

									while($row = $resultq->fetch_assoc()){ 
									if ((strcmp($row['id'],$categoryID) == 0)) {
										echo $row['id'] . $row['category'];
			    								echo "<option value=\"" .$row['id']. "\"selected>" . $row['category'] ."</option>" ;
									}else{

			    						echo $row['id'] . $row['category'];
			    								echo "<option value=\"" .$row['id']. "\">" . $row['category'] ."</option>" ;
			    							}
										} 
											
									?>
								</select>
							</fieldset>
							
							
							
				
							<fieldset style="width:48%; float:left; margin-right: 3%;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Latitude</label>
								<input type="text" name="business_latitude" value="<?php echo $line['business_latitude']; ?>" style="width:92%;" > 
								<label>Longitude</label>
								<input type="text" name="business_longitude" value="<?php echo $line['business_longitude']; ?>" style="width:92%;" > 
							</fieldset>
							
							<fieldset style="width:48%; float:left;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Contact</label>
								<input type="text" name="business_contact_num" value="<?php echo $line['business_contact_num']; ?>" style="width:92%;" > 
							</fieldset>
							
							<div class="clear"></div>

					</div>
					<footer>
						<div class="submit_link">
							<input type="submit" value="Update Branch" class="alt_btn">
							<input type="submit" name="done" value="DONE">
						</div>
					</footer>
				</form>
			
		</article><!-- end of post new article -->
		<?php }} ?>

		<article class="module width_full">
		<header><h3 class="tabs_involved">Branches</h3>
			<ul class="tabs">
	   			<li><a href="#tab1">Active</a></li>
	    		<li><a href="#tab2">Inactive</a></li>
			</ul>
		
		</header>

		<div class="tab_container">
			<div id="tab1" class="tab_content">
			<table class="tablesorter" cellspacing="0"> 
			<thead> 
				<tr> 
   		
    				<th>Business</th>
    				<th>Branch</th> 
    				<th>Address</th> 
    				<th>Contact</th> 
    				<th>Action</th> 
				</tr> 
			</thead> 
			<tbody> 
			<?php
					
			while($row = $result->fetch_assoc()){ 
				echo "<tr> " ;   		
    
    				echo "<td>" . $row['business_name'] . "</font>";
    				echo "<td>" . $row['business_location'] . "</td>";
    				echo "<td>" . $row['business_address'] . "</td>";
    				echo "<td>" . $row['business_contact_num'] . "</td>";
    				echo "<td>". "<a href=\" detailbranches.php?id=" .$row['business_id']."&rid=". $row['id']." \">"."<input type=\"image\" src=\"images/icn_edit.png\" title=\"Edit\" >" . "<a href=\" deletebranch.php?id=" .$row['id'] ."&bid=$id". " \">" ."<input type='image' src='images/icn_trash.png' title='Trash'></td> ";

				echo "</tr>"; 

			}				    

			?>
				
			</tbody> 
			</table>
			</div><!-- end of #tab1 -->

		</div><!-- end of .tab_container -->
			<div id="tab2" class="tab_content">
			<?php
				$inactive = $conn->query("SELECT `tba_businesses`.business_name,`tba_business_details`.id, `tba_business_details`.business_id,`business_category`,`business_location`,`business_address`,`business_latitude`, `business_longitude`, `business_contact_num` FROM `tba_businesses`,`tba_business_details` WHERE `tba_business_details`.business_id = `tba_businesses`.business_id AND `tba_business_details`.`business_id` =$id AND `tba_business_details`.`business_available` =0 "); 
			?>
			<table class="tablesorter" cellspacing="0"> 
			<thead> 
				<tr> 
   		
    				<th>Business</th>
    				<th>Branch</th> 
    				<th>Address</th> 
    				<th>Contact</th> 
    				<th>Action</th> 
				</tr> 
			</thead> 
			<tbody> 
			<?php
					
			while($row = $inactive->fetch_assoc()){ 
				echo "<tr> " ;   		
    
    				echo "<td>" . $row['business_name'] . "</font>";
    				echo "<td>" . $row['business_location'] . "</td>";
    				echo "<td>" . $row['business_address'] . "</td>";
    				echo "<td>" . $row['business_contact_num'] . "</td>";
    				echo "<td>". "<a href=\" activatebranch.php?id=" .$row['id'] ."&bid=$id". " \">" ."<input type='image' src='images/icn_alert_success.png' title='Trash'></td> "; ;

				echo "</tr>"; 

			}				    

			?>
				
			</tbody> 
			</table>
			</div><!-- end of #tab2 -->

		</div><!-- end of .tab_container -->
		</article>

		<div class="spacer"></div>
	</section>
</body>

</html>
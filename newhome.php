<?php
session_start();

include_once"config.php";
if(!isset($_SESSION['username']) || !isset($_SESSION['password'])){
	header("Location: login.php");
}else{
//$fetch_users_data = mysql_fetch_object(mysql_query("SELECT * FROM `members` WHERE username='".$_SESSION['username']."'"));
}
?>
<?php
header('Content-Type: text/html; charset=utf8_general_ci');
//$id = $_GET['id'];
//echo $id;

mysqli_set_charset($conn,"utf8");
//$result = $conn->query("SELECT * FROM tba_tours WHERE id = '$id'"); 

?>

<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8"/>
	<meta name="author" content="Bhawick Ghutla">

	<title>Home Featured</title>
	
	<link rel="stylesheet" href="css/layout.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="css/style.css">
	<!--[if lt IE 9]>
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="js/jquery-1.5.2.min.js" type="text/javascript"></script>
	<script src="js/hideshow.js" type="text/javascript"></script>
	<script src="js/jquery.tablesorter.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery.equalHeight.js"></script>
	<script type="text/javascript">
	$(document).ready(function() 
    	{ 
      	  $(".tablesorter").tablesorter(); 
   	 } 
	);
	$(document).ready(function() {

	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});
    </script>

    <script type="text/javascript">
    $(function(){
        $('.column').equalHeight();
    });
    
</script>
<script>
function myFunction() {
    var checkBox = document.getElementById("myCheck");
    var text = document.getElementById("rent");
    if (checkBox.checked == true){
        text.style.display = "block";
    } else {
       text.style.display = "none";
    }
}
</script>
<script>
function Function() {
    var checkBox = document.getElementById("check");
    var text = document.getElementById("tours");
    if (checkBox.checked == true){
        text.style.display = "block";
    } else {
       text.style.display = "none";
    }
}
</script>
<!-- Function for Showing FileUpload option -->
<script type="text/javascript">
    function ShowHideDiv(chkImage) {
        var dvPassport = document.getElementById("dvPassport");
        dvPassport.style.display = chkImage.checked ? "block" : "none";
    }
</script>
</head>


<body>

	<header id="header">
		<hgroup>
			<h1 class="site_title"><a href="index.php">Feejee Traveller</a></h1>
			<h2 class="section_title">Home Featured</h2>
		</hgroup>
	</header> <!-- end of header bar -->
	
	<section id="secondary_bar">
		<div class="user">
			<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
		</div>
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="index.php">Feejee Traveller</a> <div class="breadcrumb_divider"></div> <a class="current">Home Featured</a></article>
		</div>
	</section><!-- end of secondary bar -->
	
	<?php include('navBar.php'); ?>
	
	<section id="main" class="column">

		<div class="clear"></div>
		
		<article class="module width_full">
			<header><h3>Home Featured</h3></header>
				<form action="addhome.php" method="post" enctype="multipart/form-data">
					<div class="module_content">
					<!-- Data Setting starts here -->
							<p> Please Select either Tours or Rentals to display on the home featured </p>
							Rentals: <input type="checkbox" id="myCheck"  onclick="myFunction()">
							Tours: <input type="checkbox" id="check"  onclick="Function()">
							<div class="clear"></div>
							<fieldset style="width:48%; display:none;   float:left; " id="rent">
								<label>Select Rental</label>
								<select style="width:92%;" name="rental" id="rental" >
								<?php 
								$resultq = $conn->query("SELECT * FROM `rentals` WHERE `language_lang_id` = '1' ORDER BY rental_name ASC"); 

								while($row = $resultq->fetch_assoc()){ 						
	    						
	    								echo "<option value=\"" .$row['rentalmulti_id']. "\">" . $row['rental_name'] ."</option>" ;
								} 
									
								?>
								</select>
							</fieldset>
							<fieldset style="width:48%; float:right;  display:none;" id="tours">
								<label>Select Tour</label>
								<select style="width:92%;" name="tour_rental_id" id="tour_rental_id" >
								<?php 
								$resultq = $conn->query("SELECT * FROM `tours` WHERe `language_lang_id` = '1' ORDER BY tour_name ASC"); 

								while($row = $resultq->fetch_assoc()){ 						
	    						
	    								echo "<option value=\"" .$row['tourmulti_id']. "\">" . $row['tour_name'] ."</option>" ;
								} 
									
								?>
								</select>
							</fieldset>
							<div class="clear"></div>
							<fieldset style="width:48%; float:left;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Featured Type</label>
								<select style="width:92%;" name="homef_type" id="homef_type" >
								<?php 
								$resultq = $conn->query("SELECT * FROM `homef_type`"); 

								while($row = $resultq->fetch_assoc()){ 						
	    						
	    								echo "<option value=\"" .$row['homef_type_id']. "\">" . $row['homef_type_name'] ."</option>" ;
								} 
									
								?>
								</select>
							</fieldset>
							
							

							<div class="clear"></div>
							
							
							<fieldset style="width:48%; float:right; "> <!-- to make two field float next to one another, adjust values accordingly -->
							<label>Lanuage Type</label>
								<select style="width:92%;" name="language" id="language" >
								<?php 
								$resultq = $conn->query("SELECT * FROM `language`"); 

								while($row = $resultq->fetch_assoc()){ 						
	    						
	    								echo "<option value=\"" .$row['lang_id']. "\">" . $row['lang_name'] ."</option>" ;
								} 
									
								?>
								</select>
							</fieldset>
							<div class="clear"></div>
							<fieldset style="width:48%; float:leftt; ">
							<label>Link</label>
								<input type="text" name="homef_link" value="" style="width:92%;" > 
							</fieldset>

							<!-- Data Setting Ends here -->
									<div class="clear"></div>

							<div class="clear"></div>
							
					</div>
					<footer>
						<div class="submit_link">
						
							<input type="submit" value="Add" class="alt_btn">
							<input type="submit" value="Reset">
						</div>
					</footer>
				</form>
			
		</article><!-- end of post new article -->

		<div class="spacer"></div>
	</section>

</body>

</html>
<?php 
		//Connect to database
	$servername = "localhost";
	$username = "root";
	$password = "root";
	$dbname = "newswebAir";

	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);

	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 
	$result = $conn->query("SELECT * FROM Category");


?>


<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8"/>
	<title>SpideyWeb</title>
	
	<link rel="stylesheet" href="css/layout.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="css/style.css">
	<!--[if lt IE 9]>
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="js/jquery-1.5.2.min.js" type="text/javascript"></script>
	<script src="js/hideshow.js" type="text/javascript"></script>
	<script src="js/jquery.tablesorter.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery.equalHeight.js"></script>
	<script type="text/javascript">
	$(document).ready(function() 
    	{ 
      	  $(".tablesorter").tablesorter(); 
   	 } 
	);
	$(document).ready(function() {

	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});
    </script>

    <script type="text/javascript">
    $(function(){
        $('.column').equalHeight();
    });
</script>
<!-- Function for Showing FileUpload option -->
<script type="text/javascript">
    function ShowHideDiv(chkImage) {
        var dvPassport = document.getElementById("dvPassport");
        dvPassport.style.display = chkImage.checked ? "block" : "none";
    }
</script>
</head>


<body>

	<header id="header">
		<hgroup>
			<h1 class="site_title"><a href="index.html">Daily Planet Portal</a></h1>
			<h2 class="section_title">New Article </h2><div class="btn_view_site"><a href="http://www.medialoot.com">View Site</a></div>
		</hgroup>
	</header> <!-- end of header bar -->
	
	<section id="secondary_bar">
		<div class="user">
			<p>Clark Kent  (<a href="#">3 Messages</a>)</p>
			<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
		</div>
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="index.html">Website Admin</a> <div class="breadcrumb_divider"></div> <a class="current">New Article</a></article>
		</div>
	</section><!-- end of secondary bar -->
	
	<?php include('navBar.php'); ?>
	
	<section id="main" class="column">

		<div class="clear"></div>
		
		<article class="module width_3_quarter">
			<header><h3>Create New Category</h3></header>
				<form action="categoryController.php" method="post" enctype="multipart/form-data">
					<div class="module_content">
							<fieldset style="width:95%; float:left; margin-right: 3%;">
								<label>Category name</label>
								<input type="text"  name="categoryName" id="categoryName" style="float:left; margin-right: 1%; width:95%; ">

							</fieldset>
							<fieldset style="width:95%; float:left; margin-right: 3%;">
								<label>Category Description</label>
								<textarea rows="6" name="categoryDesc" id="categoryDesc"style="float:left; margin-right: 1%; width: 95%" ></textarea>
							</fieldset>
							
							<div class="clear"></div>
							
					</div>
					<footer>
						<div class="submit_link">
							<input type="submit" value="ADD" class="alt_btn">
							<input type="submit" value="Reset">
						</div>
					</footer>
				</form>
			
		</article><!-- end of post new Category -->
		<div class="clear"></div>
		<article class="module width_full">
		<header><h3 class="tabs_involved">Catergory Manager</h3>
		
		</header>



		<div class="tab_container">
			<div id="tab1" class="tab_content">
			<table class="tablesorter" cellspacing="0"> 
			<thead> 
				
				<tr> 
   					<th></th> 
    				<th>Category</th> 
    				<th>Category description</th> 
    				<th>Created On</th> 
    				<th>Actions</th> 
				</tr> 
			</thead> 
			<tbody> 

				
			
				<?php
					while($row = mysqli_fetch_array($result))
				{
					echo "<tr> " ;

   					echo "<td><input type='checkbox'></td>";
    				echo "<td>" . $row['Name'] . "</td>";
    				echo "<td>" . $row['Description'] . "</td>";
    				echo "<td>" . $row['DateCreated'] . "</td>";
    				echo"<td><input type=\"image\" src=\"images/icn_edit.png\" title=\"Edit\" >" . "<a href=\" delCategory.php?Name=" .$row['Name']. " \">" ."<input type='image' src='images/icn_trash.png' title='Trash'></td> ";

				echo "</tr>"; 
			}
			?>
				
			</tbody> 
			</table>
			</div><!-- end of #tab1 -->
			
			
		</div><!-- end of .tab_container -->
		</article>

	</section>

</body>

</html>
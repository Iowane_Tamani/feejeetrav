<?php 
		//Connect to database
	$servername = "localhost";
	$username = "root";
	$password = "root";
	$dbname = "newswebAir";


	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);

	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 
	$array = array();
?>



<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8"/>
	<title>SpideyWeb</title>
	
	<link rel="stylesheet" href="css/layout.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="css/style.css">
	<!--[if lt IE 9]>
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="js/jquery-1.5.2.min.js" type="text/javascript"></script>
	<script src="js/hideshow.js" type="text/javascript"></script>
	<script src="js/jquery.tablesorter.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery.equalHeight.js"></script>
	<script type="text/javascript">
	$(document).ready(function() 
    	{ 
      	  $(".tablesorter").tablesorter(); 
   	 } 
	);
	$(document).ready(function() {

	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});
    </script>

    <script type="text/javascript">
    $(function(){
        $('.column').equalHeight();
    });
</script>
<!-- Function for Showing FileUpload option -->
<script type="text/javascript">
    function ShowHideDiv(chkImage) {
        var dvPassport = document.getElementById("dvPassport");
        dvPassport.style.display = chkImage.checked ? "block" : "none";
    }
</script>
</head>


<body>

	<header id="header">
		<hgroup>
			<h1 class="site_title"><a href="index.html">Daily Planet Portal</a></h1>
			<h2 class="section_title">New Article </h2><div class="btn_view_site"><a href="http://www.medialoot.com">View Site</a></div>
		</hgroup>
	</header> <!-- end of header bar -->
	
	<section id="secondary_bar">
		<div class="user">
			<p>Clark Kent  (<a href="#">3 Messages</a>)</p>
			<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
		</div>
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="index.html">Website Admin</a> <div class="breadcrumb_divider"></div> <a class="current">New Article</a></article>
		</div>
	</section><!-- end of secondary bar -->
	
	<?php include('navBar.php'); ?>
	
	<section id="main" class="column">

		<div class="clear"></div>
		
		<article class="module width_full">
			<header><h3>Post New Article</h3></header>
				<form action="articleController.php" method="post" enctype="multipart/form-data">
					<div class="module_content">
							<fieldset>
								<label>Post Title</label>
								<input type="text"  name="title" id="title">
							</fieldset>
							<fieldset>
								<label>Content</label>
								<textarea rows="12" name="content" id="content"></textarea>
							</fieldset>
							<fieldset><label for="chkImage">
								<input type="checkbox" name="chkImage" id="chkImage" value="yes" onclick="ShowHideDiv(this)" />
								    Select to upload Image
								</label>
							</fieldset>
							<div id="dvPassport" style="display: none">
								<fieldset>
									
									    <script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
										    <div class="file-upload">
										      <button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Add Image</button>

										      <div class="image-upload-wrap">
										        <input class="file-upload-input" type='file' name="UploadImage" id = "UploadImage" onchange="readURL(this);" accept="image/*" />
										        <div class="drag-text">
										          <h3>Drag and drop a file or select add Image</h3>
										        </div>
										      </div>
										      <div class="file-upload-content">
										        <img class="file-upload-image" src="#" alt="your image" />
										        <div class="image-title-wrap">
										          <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
										        </div>
										      </div>
										    </div>
    
 										 <script src="js/index.js"></script>
									
								</fieldset>
							</div>
							<fieldset style="width:48%; float:left; margin-right: 3%;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Category</label>
								<select style="width:92%;" name="category" id="category" >
								<?php 
									$result = mysqli_query($conn,"SELECT * FROM Category");
									while($row = mysqli_fetch_array($result))
									{
						

	    								echo "<option value=\"" .$row['id']. "\">" . $row['Name'] ."</option>" ;
	    								
									} 
									
								?>
								</select>
							</fieldset>
							<fieldset style="width:48%; float:left;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Tags</label>
								<input type="text" name="tags" id="tags"style="width:92%;">
							</fieldset><div class="clear"></div>
							
					</div>
					<footer>
						<div class="submit_link">
							<select>
								<option>Draft</option>
								<option>Published</option>
							</select>
							<input type="submit" value="Publish" class="alt_btn">
							<input type="submit" value="Reset">
						</div>
					</footer>
				</form>
			
		</article><!-- end of post new article -->

		<div class="spacer"></div>
	</section>
</body>

</html>
-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Aug 31, 2016 at 01:05 AM
-- Server version: 5.5.42
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `rosiehol_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `tba_log`
--

CREATE TABLE `tba_log` (
  `log_id` int(11) NOT NULL,
  `log_tablename` varchar(50) NOT NULL,
  `log_itemid` int(10) NOT NULL,
  `log_actiontaken` varchar(25) NOT NULL,
  `log_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `log_username` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;



--
-- Indexes for dumped tables
--

--
-- Indexes for table `tba_log`
--
ALTER TABLE `tba_log`
  ADD PRIMARY KEY (`log_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tba_log`
--
ALTER TABLE `tba_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=0;
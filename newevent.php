<?php  
session_start();

include_once"config.php";
if(!isset($_SESSION['username']) || !isset($_SESSION['password'])){
	header("Location: login.php");
}else{
//$fetch_users_data = mysql_fetch_object(mysql_query("SELECT * FROM `members` WHERE username='".$_SESSION['username']."'"));
}
?>
<?php 
header('Content-Type: text/html; charset=utf8_general_ci');
//$id = $_GET['id'];

?>

<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8"/>
	<meta name="author" content="Bhawick Ghutla">

	<title>New Event</title>
	
	<link rel="stylesheet" href="css/layout.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="css/style.css">
	<!--[if lt IE 9]>
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="js/jquery-1.5.2.min.js" type="text/javascript"></script>
	<script src="js/hideshow.js" type="text/javascript"></script>
	<script src="js/jquery.tablesorter.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery.equalHeight.js"></script>
	<script type="text/javascript">
	$(document).ready(function() 
    	{ 
      	  $(".tablesorter").tablesorter(); 
   	 } 
	);
	$(document).ready(function() {

	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});
    </script>

    <script type="text/javascript">
    $(function(){
        $('.column').equalHeight();
    });
</script>
<!-- Function for Showing FileUpload option -->
<script type="text/javascript">
    function ShowHideDiv(chkImage) {
        var dvPassport = document.getElementById("dvPassport");
        dvPassport.style.display = chkImage.checked ? "block" : "none";
    }
</script>
</head>


<body>

	<header id="header">
		<hgroup>
			<h1 class="site_title"><a href="index.php">Feejee Traveller</a></h1>
			<h2 class="section_title">New Event </h2><
		</hgroup>
	</header> <!-- end of header bar -->
	
	<section id="secondary_bar">
		<div class="user">
			<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
		</div>
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="index.php">Feejee Traveller</a> <div class="breadcrumb_divider"></div> <a class="current">New Event</a></article>
		</div>
	</section><!-- end of secondary bar -->
	
	<?php include('navBar.php'); ?>
	
	<section id="main" class="column">

		<div class="clear"></div>
		
		<article class="module width_full">
			<header><h3>Add New Event</h3></header>
				<form action="addevt.php" method="post" enctype="multipart/form-data">
					<div class="module_content">
					<!-- Data Setting starts here -->
					
							<fieldset style="width:48%; float:left; margin-right: 3%;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Event Name </label>
								<input type="text" name="event_name" value="" style="width:92%;" > 
								
							</fieldset>
							<!--<fieldset style="width:48%; float:left;"> 
								<label>Event Name Chinese</label>
								<input type="text" name="eventName_zh" value="" style="width:92%;">
							</fieldset>-->
							<fieldset style="width:48%; float:left; "> <!-- to make two field float next to one another, adjust values accordingly -->
							<label>Lanuage Type</label>
								<select style="width:92%;" name="language_lang_id" id="language_lang_id" >
								<?php 
								$resultq = $conn->query("SELECT * FROM `language`"); 

								while($row = $resultq->fetch_assoc()){ 						
	    						
	    								echo "<option value=\"" .$row['lang_id']. "\">" . $row['lang_name'] ."</option>" ;
								} 
									
								?>
								</select>
							</fieldset>
							<div class="clear"></div>	
							<fieldset style="width:100%; float:left;">
								
								<fieldset style="width:20%; float:left;"> 
							
										<label> Event Start Date </label>
											<input type = "date" name="event_startdate" style="width:92%; float: left; margin-left: 3%;" >
								</fieldset>
								<fieldset style="width:20%; float:left;"> 
										<label>Event End Date </label>
										
											<input type = "date" name="event_enddate" style="width:92%; float: left; margin-left: 3%;" >
								</fieldset>

								
							</fieldset>







					<!-- Data Setting Ends here -->

							<fieldset style="width:100%; float:left;">
								<label for="chkImage">
								<input type="checkbox" name="chkImage" id="chkImage" value="yes" onclick="ShowHideDiv(this)" />
								    Select to upload Image
								</label>
							</fieldset>
							<div id="dvPassport" style="display: none">
								<fieldset>
									
									    <script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
										    <div class="file-upload">
										      <button class="file-upload-btn" type="button" onclick="$('.file-upload-input').trigger( 'click' )">Add Image</button>

										      <div class="image-upload-wrap">
										        <input class="file-upload-input" type='file' name="UploadImage" id = "UploadImage" onchange="readURL(this);" accept="image/*" />
										        <div class="drag-text">
										          <h3>Drag and drop a file or select add Image</h3>
										        </div>
										      </div>
										      <div class="file-upload-content">
										        <img class="file-upload-image" src="#" alt="your image" />
										        <div class="image-title-wrap">
										          <button type="button" onclick="removeUpload()" class="remove-image">Remove <span class="image-title">Uploaded Image</span></button>
										        </div>
										      </div>
										    </div>
    
 										 <script src="js/index.js"></script>
									
								</fieldset>
							</div>
							
							<div class="clear"></div>
							
					</div>
					<footer>
						<div class="submit_link">

							<input type="submit" value="Add Event" class="alt_btn">
							<input type="submit" value="Reset">
						</div>
					</footer>
				</form>
			
		</article><!-- end of post new article -->

		<div class="spacer"></div>
	</section>
</body>

</html>
<?php 
session_start();

include_once"config.php";
if(!isset($_SESSION['username']) || !isset($_SESSION['password'])){
	header("Location: login.php");
}else{
//$fetch_users_data = mysql_fetch_object(mysql_query("SELECT * FROM `members` WHERE username='".$_SESSION['username']."'"));
}
?>

<?php 
         $left_rec = 0;
         $rec_count = 0;
         $page = 0;
         $rec_limit = 0;
         
        $left_rec = $rec_count - ($page * $rec_limit);
        mysqli_set_charset($conn,"utf8");
		$result = $conn->query("SELECT `trates_id`, `tours_multitour_id`,`tours`.tour_name,`trates_prenote`,`trates_type`,`trates_cost`,`trates_display` FROM `tour_rates`,`tours` WHERE `tour_rates`.`tours_multitour_id` = `tours`.tourmulti_id GROUP BY `trates_id` ") ; 
					
?>
<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8"/>
	<meta name="author" content="Bhawick Ghutla">

	<title>Tour Rates</title>
	
	<link rel="stylesheet" href="css/layout.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="css/style.css">
	<!--[if lt IE 9]>
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="js/jquery-1.5.2.min.js" type="text/javascript"></script>
	<script src="js/hideshow.js" type="text/javascript"></script>
	<script src="js/jquery.tablesorter.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery.equalHeight.js"></script>
	<script type="text/javascript">
	$(document).ready(function() 
    	{ 
      	  $(".tablesorter").tablesorter(); 
   	 } 
	);
	$(document).ready(function() {

	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});
    </script>

    <script type="text/javascript">
    $(function(){
        $('.column').equalHeight();
    });
</script>
<!-- Function for Showing FileUpload option -->
<script type="text/javascript">
    function ShowHideDiv(chkImage) {
        var dvPassport = document.getElementById("dvPassport");
        dvPassport.style.display = chkImage.checked ? "block" : "none";
    }
</script>
</head>


<body>

	<header id="header">
		<hgroup>
			<h1 class="site_title"><a href="index.php">Feejee Traveller</a></h1>
			<h2 class="section_title">RATES </h2>
		</hgroup>
	</header> <!-- end of header bar -->
	
	<section id="secondary_bar">
		<div class="user">
			<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
		</div>
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="index.php">Feejee Traveller</a> <div class="breadcrumb_divider"></div> <a class="current">Rates</a></article>
		</div>
	</section><!-- end of secondary bar -->
	<!-- including navigation Bar -->
	<?php include('navBar.php'); ?>
	
	<section id="main" class="column">

		<div class="clear"></div>
		
		<article class="module width_full">
		<header><h3 class="tabs_involved">Content Manager</h3>
		
		</header>

		<div class="tab_container">
			<div id="tab1" class="tab_content">
			<table class="tablesorter" cellspacing="0"> 
			<thead> 
				<tr> 
   		
    				<th>Entry Name</th> 

    				<th>Category</th> 
    				<th>Cost</th> 
    				<th>Actions</th> 
				</tr> 
			</thead> 
			<tbody> 
			<?php
					
			while($row = $result->fetch_assoc()){ 
				echo "<tr> " ;

   					//echo "<td><input type='checkbox'></td>";
   		
    
    				echo "<td><font size=\"4\">" . $row['tour_name'] . "</font>";
    				
    				echo "<td>" . $row['trates_type'] . "</td>";
    				//echo "<td> $<input type=\"text\"  value=\"". $row['rate_cost']."\" name = \"rate_cost\"></td>";
    				echo "<td>" . $row['trates_cost'] . "</td>";
    				echo"<td>". "<a href=\" detailrates.php?id=" .$row['tours_multitour_id']."&rid=". $row['trates_id']." \">" ."<input type=\"image\" src=\"images/icn_edit.png\" title=\"Edit\" >" . "<a href=\" deleterate.php?id=" .$row['trates_id']. " \">" ."<input type='image' src='images/icn_trash.png' title='Trash'></td> ";

				echo "</tr>"; 

			}
?>
				
			</tbody> 
			</table>
			</div><!-- end of #tab1 -->
			
		</div><!-- end of .tab_container -->
		
		</article><!-- end of content manager article -->
		
		<div class="spacer"></div>
	</section>
</body>

</html>
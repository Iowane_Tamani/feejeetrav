<?php
session_start();

include_once"config.php";
if(!isset($_SESSION['username']) || !isset($_SESSION['password'])){
	header("Location: login.php");
}else{
//$fetch_users_data = mysql_fetch_object(mysql_query("SELECT * FROM `members` WHERE username='".$_SESSION['username']."'"));
}
?>
<?php
header('Content-Type: text/html; charset=utf8_general_ci');
$id = $_GET['id'];
echo $id;
?>
<?php

        mysqli_set_charset($conn,"utf8");
		$result = $conn->query("SELECT `tourratemulti_id`,`tours`.tour_name,`trates_prenote`,`trates_type`,`trates_cost`,`trates_display`,`tourmulti_id` FROM `tour_rates`,`tours` WHERE `tour_rates`.`tours_multitour_id` = `tours`.tourmulti_id AND `tourmulti_id` = $id AND `tour_rates`.`trates_display` = 1 GROUP BY tourratemulti_id ");
		$sql = $conn->query("SELECT * FROM `tour_rates` WHERE `tours_multitour_id` = $id"); 
?>

<!doctype html>
<html lang="en">

<head>
	<meta charset="utf-8"/>

	<title>Edit Tour Rates </title>
	
	<link rel="stylesheet" href="css/layout.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="css/style.css">
	<!--[if lt IE 9]>
	<link rel="stylesheet" href="css/ie.css" type="text/css" media="screen" />
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script src="js/jquery-1.5.2.min.js" type="text/javascript"></script>
	<script src="js/hideshow.js" type="text/javascript"></script>
	<script src="js/jquery.tablesorter.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/jquery.equalHeight.js"></script>
	<script type="text/javascript">
	$(document).ready(function() 
    	{ 
      	  $(".tablesorter").tablesorter(); 
   	 } 
	);
	$(document).ready(function() {

	//When page loads...
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content

	//On Click Event
	$("ul.tabs li").click(function() {

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the href attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active ID content
		return false;
	});

});
    </script>

    <script type="text/javascript">
    $(function(){
        $('.column').equalHeight();
    });
</script>
<!-- Function for Showing FileUpload option -->
<script type="text/javascript">
    function ShowHideDiv(chkImage) {
        var dvPassport = document.getElementById("dvPassport");
        dvPassport.style.display = chkImage.checked ? "block" : "none";
    }
</script>
</head>


<body>

	<header id="header">
		<hgroup>
			<h1 class="site_title"><a href="index.php">Feejee Traveller</a></h1>
			<h2 class="section_title">Edit Tour Rates</h2>
		</hgroup>
	</header> <!-- end of header bar -->
	
	<section id="secondary_bar">
		<div class="user">
			<!-- <a class="logout_user" href="#" title="Logout">Logout</a> -->
		</div>
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a href="index.php">Feejee Traveller</a> <div class="breadcrumb_divider"></div> <a class="current">Rates</a></article>
		</div>
	</section><!-- end of secondary bar -->
	
	<?php include('navBar.php'); ?>
	

		
		
		
		
	
	<section id="main" class="column">
		<article class="module width_full">
		<header><h3 class="tabs_involved">Content Manager</h3>
			<ul class="tabs">
   			<li><a href="#tab1">Active</a></li>
    		
		</ul>
		</header>

		<div class="tab_container">
			<div id="tab1" class="tab_content">
			<table class="tablesorter" cellspacing="0"> 
			<thead> 
				<tr> 
   		
    				<th>Tour Name</th> 

    				<th>Description</th> 
    				<th>Type</th> 
    				<th>Cost</th> 
    				<th>Action</th> 
				</tr> 
			</thead> 
			<tbody> 
			<?php
			
						
							
			while($row =$result->fetch_assoc()){ 
				echo "<tr> " ;   		
    
    				echo "<td>" . $row['tour_name'] . "</font>";
    				echo "<td>" . $row['trates_prenote'] . "</td>";
    				echo "<td>" . $row['trates_type'] . "</td>";
    				echo "<td>$" . $row['trates_cost'] . "</td>";
    				echo "<td>". "<a href=\" detailrates.php?id=" .$row['tourmulti_id']." \">"."<input type=\"image\" src=\"images/icn_edit.png\" title=\"Edit\" >";

				echo "</tr>"; 

			}				    

?>
				
			</tbody> 
			</table>
			</div><!-- end of #tab1 -->
			
		</div><!-- end of .tab_container -->
		
		</article><!-- end of content manager article -->
		<div class="clear"></div>
		<?php 			
			if ($rid !== null) {
			while($line =$sql->fetch_assoc()){ 
												
		?>
		<article class="module width_full">
			<header><h3>Tour</h3></header>
				<form action="editrates.php?rid= <?php echo $rid. "&id=$id";?> " method="post" enctype="multipart/form-data">
					<div class="module_content">
					<!-- Data Setting starts here -->
					
							<fieldset style="width:48%; float:left; margin-right: 3%;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Rates Pre note </label>
								<input type="text" name="rate_pre_note" value="<?php echo $line['rate_pre_note']; ?>" style="width:92%;" > 
								
							</fieldset>
							<fieldset style="width:48%; float:left;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Rates Pre note Chinese</label>
								<input type="text" name="rate_pre_note_zh" value="<?php echo $line['rate_pre_note_zh']; ?>" style="width:92%;">
							</fieldset><div class="clear"></div>	
							
							<fieldset style="width:100%; float:left; margin-right: 3%;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Pickup Location </label>
								<input type="text" name="rate_pickup" value="<?php echo $line['rate_pickup']; ?>" style="width:92%;" > 
								
							</fieldset>
							
				
							<fieldset style="width:48%; float:left; margin-right: 3%;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Rate Post Note</label>
								<input type="text" name="rate_post_note" value="<?php echo $line['rate_post_note']; ?>" style="width:92%;" > 
								<label>Rate Post Note Chinese</label>
								<input type="text" name="rate_post_note_zh" value="<?php echo $line['rate_post_note_zh']; ?>" style="width:92%;" > 
							</fieldset>
							
							<fieldset style="width:48%; float:left;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>Rate Type</label>
								<input type="text" name="rate_type" value="<?php echo $line['rate_type']; ?>" style="width:92%;" > 
								<label>Rate Type Chinese</label>
								<input type="text" name="rate_type_zh" value="<?php echo $line['rate_type_zh']; ?>" style="width:92%;" > 
							</fieldset>
							<fieldset style="width:48%; float:left;"> <!-- to make two field float next to one another, adjust values accordingly -->
								<label>PRICE</label>
								<input type="text" name="rate_cost" value="<?php echo $line['rate_cost']; ?>" style="width:92%;" > 
							</fieldset>
							<div class="clear"></div>
					</div>
					<footer>
						<div class="submit_link">
							<input type="submit" value="Update Rate" class="alt_btn">
							<input type="submit" name="done" value="DONE">
						</div>
					</footer>
				</form>
			
		</article><!-- end of post new article -->
		<?php } 
		}
		?>

		<div class="spacer"></div>
	</section>
</body>

</html>